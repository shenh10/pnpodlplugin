package org.opendaylight.controller.pnp.objects.assistant;

import java.lang.reflect.InvocationTargetException;

import javax.xml.bind.JAXBElement;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.classes.imageInstallDir.Pnp;
import org.opendaylight.controller.pnp.objects.classes.imageInstallDir.imageInstall.Request;
import org.opendaylight.controller.pnp.objects.classes.imageInstallDir.imageInstall.ObjectFactory;
import org.opendaylight.controller.pnp.objects.impl.CustomedConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class ImageInstallAssistant extends  AssistantParent{
    private final static long serialVersionUID = 1L;
    protected static final Logger logger = LoggerFactory
        .getLogger(ImageInstallAssistant.class);
    protected Integer noCheckTime;
    protected String postReloadPriv;
    protected String srcLocation;
    protected String srcUri;
    protected String srcChecksum;

    protected String desLocation;
    protected String desUri;
    protected Boolean isReload;
    protected String reason;
    protected Integer delayIn;
    protected String user;
    protected Boolean isSaveConfig;

    public void setNoCheckTime(Integer value){

        this.noCheckTime = value;

    }

    public Integer getNoCheckTime(){

        return this.noCheckTime;
    }
    public void setPostReloadPriv(String value){

        this.postReloadPriv = value;

    }

    public String getPostReloadPriv(){

        return this.postReloadPriv;
    }
    public void setSrcLocation(String value){

        this.srcLocation = value;

    }

    public String getSrcLocation(){

        return this.srcLocation;
    }
    public void setSrcUri(String value){

        this.srcUri = value;

    }

    public String getSrcUri(){

        return this.srcUri;
    }
    public void setSrcChecksum(String value){

        this.srcChecksum = value;

    }

    public String getSrcChecksum(){

        return this.srcChecksum;
    }
    public void setDesLocation(String value){

        this.desLocation = value;

    }

    public String getDesLocation(){

        return this.desLocation;
    }
    public void setDesUri(String value){

        this.desUri = value;

    }

    public String getDesUri(){

        return this.desUri;
    }
    public void setIsReload(Boolean value){

        this.isReload = value;

    }

    public Boolean getIsReload(){

        return this.isReload;
    }
    public void setReason(String value){

        this.reason = value;

    }

    public String getReason(){

        return this.reason;
    }
    public void setDelayIn(Integer value){

        this.delayIn = value;

    }

    public Integer getDelayIn(){

        return this.delayIn;
    }
    public void setUser(String value){

        this.user = value;

    }

    public String getUser(){

        return this.user;
    }
    public void setIsSaveConfig(Boolean value){

        this.isSaveConfig = value;

    }

    public Boolean getIsSaveConfig(){

        return this.isSaveConfig;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result= prime*result+((noCheckTime == null) ? 0 : noCheckTime.hashCode());

        result= prime*result+((postReloadPriv == null) ? 0 : postReloadPriv.hashCode());

        result= prime*result+((srcLocation == null) ? 0 : srcLocation.hashCode());

        result= prime*result+((srcUri == null) ? 0 : srcUri.hashCode());

        result= prime*result+((srcChecksum == null) ? 0 : srcChecksum.hashCode());

        result= prime*result+((desLocation == null) ? 0 : desLocation.hashCode());

        result= prime*result+((desUri == null) ? 0 : desUri.hashCode());

        result= prime*result+((isReload == null) ? 0 : isReload.hashCode());

        result= prime*result+((reason == null) ? 0 : reason.hashCode());

        result= prime*result+((delayIn == null) ? 0 : delayIn.hashCode());

        result= prime*result+((user == null) ? 0 : user.hashCode());

        result= prime*result+((isSaveConfig == null) ? 0 : isSaveConfig.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } ImageInstallAssistant other = (ImageInstallAssistant) obj;
        if (noCheckTime == null) {
            if (other.getNoCheckTime() != null) {
                return false;
            }
        } else if (!noCheckTime.equals(other.getNoCheckTime())) {
            return false;
        }

        if (postReloadPriv == null) {
            if (other.getPostReloadPriv() != null) {
                return false;
            }
        } else if (!postReloadPriv.equals(other.getPostReloadPriv())) {
            return false;
        }

        if (srcLocation == null) {
            if (other.getSrcLocation() != null) {
                return false;
            }
        } else if (!srcLocation.equals(other.getSrcLocation())) {
            return false;
        }

        if (srcUri == null) {
            if (other.getSrcUri() != null) {
                return false;
            }
        } else if (!srcUri.equals(other.getSrcUri())) {
            return false;
        }

        if (srcChecksum == null) {
            if (other.getSrcChecksum() != null) {
                return false;
            }
        } else if (!srcChecksum.equals(other.getSrcChecksum())) {
            return false;
        }

        if (desLocation == null) {
            if (other.getDesLocation() != null) {
                return false;
            }
        } else if (!desLocation.equals(other.getDesLocation())) {
            return false;
        }

        if (desUri == null) {
            if (other.getDesUri() != null) {
                return false;
            }
        } else if (!desUri.equals(other.getDesUri())) {
            return false;
        }

        if (isReload == null) {
            if (other.getIsReload() != null) {
                return false;
            }
        } else if (!isReload.equals(other.getIsReload())) {
            return false;
        }

        if (reason == null) {
            if (other.getReason() != null) {
                return false;
            }
        } else if (!reason.equals(other.getReason())) {
            return false;
        }

        if (delayIn == null) {
            if (other.getDelayIn() != null) {
                return false;
            }
        } else if (!delayIn.equals(other.getDelayIn())) {
            return false;
        }

        if (user == null) {
            if (other.getUser() != null) {
                return false;
            }
        } else if (!user.equals(other.getUser())) {
            return false;
        }

        if (isSaveConfig == null) {
            if (other.getIsSaveConfig() != null) {
                return false;
            }
        } else if (!isSaveConfig.equals(other.getIsSaveConfig())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString(){
        return "[ noCheckTime= :{" + noCheckTime + "} postReloadPriv= :{" + postReloadPriv
            + "} srcLocation= :{" + srcLocation + "} srcUri= :{" + srcUri
            + "} srcChecksum= :{" + srcChecksum + "} desLocation= :{" + desLocation
            + "} desUri= :{" + desUri + "} reload= :{" + isReload + "} reason= :{" + reason + "}"
            + " delayIn= :{" + delayIn + "} user= :{" + user
            + "} isSaveConfig= :{" + isSaveConfig + "} }]";
    }
    @Override
    public Object toFeature() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        CustomedConstructor constructor = new CustomedConstructor();
        Pnp obj = (Pnp)constructor.newInstance(Pnp.class);
        if(this.noCheckTime!=null)
            obj.setNoCheckTime(noCheckTime);
        if(this.postReloadPriv!=null)
            obj.setPostReloadPriv(postReloadPriv);
        Request req = (Request) obj.getRequestOrResponse();
        if(req == null){
            req = (Request) constructor.newInstance(Request.class);
            if(req == null){
                return null;
            }
        }
        Request.Image.Copy copy = req.getImage().getCopy();
        if(copy == null){
            copy = (Request.Image.Copy)constructor.newInstance(Request.Image.Copy.class);
            if(copy == null)
                return null;
        }
        Request.Image.Copy.Source src = copy.getSource();
        if(src == null){
            src = (Request.Image.Copy.Source)constructor.newInstance(Request.Image.Copy.Source.class);
            if(src == null)
                return null;
        }
        ObjectFactory objFactory = new ObjectFactory();
        if(objFactory == null){
            logger.error("Instance of {} allocate failure", ObjectFactory.class);
            return null;
        }
        if(this.srcLocation!=null)
        {
            JAXBElement<String> jaxbElement= objFactory.createRequestImageCopySourceLocation(this.srcLocation);
            src.setLocationOrUri(jaxbElement);
        }
        if(this.srcUri!=null)
        {
            JAXBElement<String> jaxbElement= objFactory.createRequestImageCopySourceLocation(this.srcUri);
            src.setLocationOrUri(jaxbElement);
        }
        if(this.srcChecksum!=null)
        {
            src.setChecksum(this.srcChecksum);
        }
        if(this.desLocation!=null || this.desUri!=null)
        {
            Request.Image.Copy.Destination des= objFactory.createRequestImageCopyDestination();
            JAXBElement<String> jaxbElement = null;
            if(this.desLocation != null){
                jaxbElement= objFactory.createRequestImageCopySourceLocation(this.desLocation);
            }
            if(this.desUri!=null){
                jaxbElement= objFactory.createRequestImageCopySourceLocation(this.desUri);
            }
            des.setLocationOrUri(jaxbElement);
        }
        if(this.isReload!=null){
            if(this.isReload == true){
                Request.Reload reloadObj = objFactory.createRequestReload();
                reloadObj.setDelayIn(this.delayIn);
                reloadObj.setReason(this.reason);
                reloadObj.setSaveConfig(this.isSaveConfig);
                reloadObj.setUser(this.user);
                req.setReloadOrNoReload(reloadObj);
            }else
                req.setReloadOrNoReload("");
        }
        return obj;
    }

}
