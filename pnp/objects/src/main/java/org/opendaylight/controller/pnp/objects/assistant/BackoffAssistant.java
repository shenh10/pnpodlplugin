package org.opendaylight.controller.pnp.objects.assistant;

import java.lang.reflect.InvocationTargetException;

import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.classes.backoffDir.Pnp;
import org.opendaylight.controller.pnp.objects.classes.backoffDir.backoff.Request;
import org.opendaylight.controller.pnp.objects.classes.backoffDir.backoff.ObjectFactory;
import org.opendaylight.controller.pnp.objects.impl.CustomedConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackoffAssistant extends  AssistantParent{
    private final static long serialVersionUID = 1L;
    protected static final Logger logger = LoggerFactory
            .getLogger(BackoffAssistant.class);
    protected String reason;
    protected String terminate;
    protected Integer defaultMinutes;
    protected Integer bfChoice;
    protected Integer hours;
    protected Integer minutes;
    protected Integer seconds;
    public void setReason(String value){

        this.reason = value;

    }

    public String getReason(){

        return this.reason;
    }
    public void setTerminate(String value){

        this.terminate = value;

    }

    public String getTerminate(){

        return this.terminate;
    }
    public void setDefaultMinutes(Integer value){

        this.defaultMinutes = value;

    }

    public Integer getDefaultMinutes(){

        return this.defaultMinutes;
    }
    public void setBfChoice(Integer value){

        this.bfChoice = value;

    }

    public Integer getBfChoice(){

        return this.bfChoice;
    }
    public void setHours(Integer value){

        this.hours = value;

    }

    public Integer getHours(){

        return this.hours;
    }
    public void setMinutes(Integer value){

        this.minutes = value;

    }

    public Integer getMinutes(){

        return this.minutes;
    }
    public void setSeconds(Integer value){

        this.seconds = value;

    }

    public Integer getSeconds(){

        return this.seconds;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result= prime*result+((reason == null) ? 0 : reason.hashCode());
        result= prime*result+((terminate == null) ? 0 : terminate.hashCode());

        result= prime*result+((defaultMinutes == null) ? 0 : defaultMinutes.hashCode());

        result= prime*result+((bfChoice == null) ? 0 : bfChoice.hashCode());

        result= prime*result+((hours == null) ? 0 : hours.hashCode());

        result= prime*result+((minutes == null) ? 0 : minutes.hashCode());

        result= prime*result+((seconds == null) ? 0 : seconds.hashCode());

        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } BackoffAssistant other = (BackoffAssistant) obj;

        if (reason == null) {
            if (other.getReason() != null) {
                return false;
            }
        } else if (!reason.equals(other.getReason())) {
            return false;
        }

        if (terminate == null) {
            if (other.getTerminate() != null) {
                return false;
            }
        } else if (!terminate.equals(other.getTerminate())) {
            return false;
        }

        if (defaultMinutes == null) {
            if (other.getDefaultMinutes() != null) {
                return false;
            }
        } else if (!defaultMinutes.equals(other.getDefaultMinutes())) {
            return false;
        }

        if (bfChoice == null) {
            if (other.getBfChoice() != null) {
                return false;
            }
        } else if (!bfChoice.equals(other.getBfChoice())) {
            return false;
        }

        if (hours == null) {
            if (other.getHours() != null) {
                return false;
            }
        } else if (!hours.equals(other.getHours())) {
            return false;
        }

        if (minutes == null) {
            if (other.getMinutes() != null) {
                return false;
            }
        } else if (!minutes.equals(other.getMinutes())) {
            return false;
        }

        if (seconds == null) {
            if (other.getSeconds() != null) {
                return false;
            }
        } else if (!seconds.equals(other.getSeconds())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString(){
        return "[ reason=: {" + reason +"} bfChoice= :{" + bfChoice +"} terminate= :{" + terminate + "} defaultMinutes= :{" + defaultMinutes  + "} hours= :{" + hours + "} minutes= :{" + minutes + "} seconds= :{" + seconds + "} }]"; }

    @Override
    public Object toFeature() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        CustomedConstructor constructor = new CustomedConstructor();
        Pnp obj = (Pnp)constructor.newInstance(Pnp.class);
        if(obj == null){
            return null;
        }
        Request req=(Request) obj.getRequestOrResponse();
        Request.Backoff backoff = req.getBackoff();
        ObjectFactory objFactory = new ObjectFactory();
        if(reason != null)
            backoff.setReason(reason);

        if(bfChoice !=null && bfChoice == 0 ){
            backoff.setTerminateOrDefaultMinutesOrCallbackAfter((terminate == null? "":terminate));
        }
        if(bfChoice !=null &&bfChoice == 1 ){
            backoff.setTerminateOrDefaultMinutesOrCallbackAfter((defaultMinutes == null? 30:defaultMinutes));

        }
        if(bfChoice !=null &&bfChoice == 2){
            Request.Backoff.CallbackAfter callBack = objFactory.createRequestBackoffCallbackAfter();
            if(hours !=null){
                callBack.setHours(hours);
            }
            if(minutes !=null){
                callBack.setMinutes(minutes);

            }
            if(seconds !=null){
                callBack.setSeconds(seconds);

            }
            backoff.setTerminateOrDefaultMinutesOrCallbackAfter(callBack);

        }


        return obj;

    }
}
