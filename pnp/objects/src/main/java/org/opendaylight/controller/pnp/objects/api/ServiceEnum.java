package org.opendaylight.controller.pnp.objects.api;
public enum ServiceEnum {
    ODL_PNP_DEVICEID(
            "device-id",
            org.opendaylight.controller.pnp.objects.classes.deviceIdDir.Pnp.class, null),
    ODL_PNP_WORKINFO(
            "work-info",
            org.opendaylight.controller.pnp.objects.classes.workInfoDir.Pnp.class,null),
    ODL_PNP_DEVICEINFO(
            "device-info",
            org.opendaylight.controller.pnp.objects.classes.deviceInfoDir.Pnp.class,
            org.opendaylight.controller.pnp.objects.assistant.DeviceInfoAssistant.class),
    ODL_PNP_IMAGE_INSTALL(
            "image-install",
            org.opendaylight.controller.pnp.objects.classes.imageInstallDir.Pnp.class,
            org.opendaylight.controller.pnp.objects.assistant.ImageInstallAssistant.class),
    ODL_PNP_CONFIG_UPGRADE(
            "config-upgrade",
            org.opendaylight.controller.pnp.objects.classes.configUpgradeDir.Pnp.class,
            org.opendaylight.controller.pnp.objects.assistant.ConfigUpgradeAssistant.class),
    ODL_PNP_RELOAD(
            "reload",
            org.opendaylight.controller.pnp.objects.classes.reloadDir.Pnp.class,
            org.opendaylight.controller.pnp.objects.assistant.ReloadAssistant.class),
    ODL_PNP_FAULT(
            "fault",
            org.opendaylight.controller.pnp.objects.classes.faultDir.Pnp.class,
            org.opendaylight.controller.pnp.objects.assistant.FaultAssistant.class),
    ODL_PNP_BACKOFF(
            "backoff",
            org.opendaylight.controller.pnp.objects.classes.backoffDir.Pnp.class,
            org.opendaylight.controller.pnp.objects.assistant.BackoffAssistant.class);
    private String service;
    private Class<?> cls;
    private Class<?> assistant;

    private ServiceEnum(String ser, Class<?> cls, Class<?> assistant) {
        this.service = ser;
        this.cls = cls;
        this.assistant = assistant;
    }

    public String getService() {
        return this.service;
    }

    public Class<?> getCls() {
        return this.cls;
    }
    public Class<?> getAssistant() {
        return this.assistant;
    }
}
