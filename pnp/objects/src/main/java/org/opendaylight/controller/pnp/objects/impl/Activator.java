package org.opendaylight.controller.pnp.objects.impl;

import org.apache.felix.dm.Component;
import org.opendaylight.controller.clustering.services.IClusterGlobalServices;
import org.opendaylight.controller.configuration.IConfigurationService;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.api.ICustomedConstructor;
import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.objects.api.PnpXmlHandler;
import org.opendaylight.controller.sal.core.ComponentActivatorAbstractBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Activator extends ComponentActivatorAbstractBase{
    protected static final Logger logger = LoggerFactory
        .getLogger(Activator.class);
    @Override
    public Object[] getImplementations() {
        return new Object[]{};
    }
    @Override
    public void configureInstance(Component c, Object imp, String containerName) {
    }
    @Override
    protected Object[] getGlobalImplementations() {
        Object[] res = { XMLHandler.class , ServiceDict.class, CustomedConstructor.class};
        return res;
    }
    @Override
    protected void configureGlobalInstance(Component c, Object imp) {
        if (imp.equals(XMLHandler.class)) {

            // export the service
            c.setInterface(new String[] {
                PnpXmlHandler.class.getName() }, null);

        }
        if (imp.equals(ServiceDict.class)) {

            // export the service
            c.setInterface(new String[] {
                PnpServiceList.class.getName() }, null);

        }
        if(imp.equals(CustomedConstructor.class)){
            c.setInterface(new String[] {
                ICustomedConstructor.class.getName() }, null);
        }

    }
}
