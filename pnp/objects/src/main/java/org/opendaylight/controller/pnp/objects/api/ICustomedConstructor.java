package org.opendaylight.controller.pnp.objects.api;

import java.lang.reflect.InvocationTargetException;

public interface ICustomedConstructor {

    public Object newInstance(Class<?> cls) throws InstantiationException,
           IllegalAccessException, IllegalArgumentException,
           InvocationTargetException, NoSuchMethodException, SecurityException;



}
