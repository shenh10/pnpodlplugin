package org.opendaylight.controller.pnp.objects.impl;

import java.util.HashMap;
import java.util.Map;

import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.objects.api.ServiceEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceDict implements PnpServiceList {
	 protected static final Logger logger = LoggerFactory
		        .getLogger(ServiceDict.class);
    private Map<String, Class<?>> map;
    private Map<String, Class<?>> assistantMap;
    public ServiceDict(){
        this.populate();
    }
    @Override
    public void populate() {
        map = new HashMap<>();
        assistantMap = new HashMap<>();
        for (ServiceEnum iter : ServiceEnum.values()) {
            if(!(map.containsKey(iter.getService())))
                map.put(iter.getService(), iter.getCls());
            if(!(assistantMap.containsKey(iter.getAssistant())))
                assistantMap.put(iter.getService(), iter.getAssistant());
        }
    }

    @Override
    public Class<?> getService(String str) {
        if(map.containsKey(str))
            return map.get(str);
        else
            return null;
    }
    @Override
    public Class<?> getAssistant(String str){
        if(assistantMap.containsKey(str))
            return assistantMap.get(str);
        else
            return null;
    }
    @Override
    public Boolean containsService(String str){
        if(map.containsKey(str))
            return true;
        else return false;
    }
    @Override
    public Map<String,Class<?>> getPnPDict(){
        return map;
    }

    @Override
    public Map<String,Class<?>> getPnPAssistantDict(){
        return assistantMap;
    }
}
