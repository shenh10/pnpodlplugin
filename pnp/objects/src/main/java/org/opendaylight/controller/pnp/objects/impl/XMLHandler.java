package org.opendaylight.controller.pnp.objects.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.opendaylight.controller.pnp.objects.api.PnpXmlHandler;
import org.opendaylight.controller.pnp.objects.api.UnQualifiedXMLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLHandler implements PnpXmlHandler{
    private ServiceDict serviceMap;
    protected static final Logger logger = LoggerFactory
        .getLogger(XMLHandler.class);

    public XMLHandler() {
        serviceMap = new ServiceDict();

    }

    @SuppressWarnings("deprecation")
    @Override
    public Object xmlParser(String string, String service) throws Exception {
        if(serviceMap.containsService(service)){
            JAXBContext jaxbContext = JAXBContext.newInstance(serviceMap.getService(service));
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader reader = new StringReader(string);
            ValidationEventCollector vec = new ValidationEventCollector();
            unmarshaller.setEventHandler( vec );
            logger.info("Unmarshall xml string against schema");
            return unmarshaller.unmarshal(reader);
        }else{
            logger.error("Not supported feature: {}", service);
            return null;
        }
    }
    @Override
    public String parseService(String str) throws ParserConfigurationException, SAXException, IOException, UnQualifiedXMLException{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = null;

        builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(str));
        Document doc = null;

        doc = builder.parse(is);
        logger.debug("Search xml for 'request' or 'response' or 'info' tag...");
        NodeList nodes = doc.getElementsByTagName("request");
        if (nodes.getLength() == 0) {
            nodes = doc.getElementsByTagName("response");

        }
        if (nodes.getLength() == 0) {
            nodes = doc.getElementsByTagName("info");

        }
        if( nodes.getLength() == 0){
            logger.error("XML {} Illegal: No any of 'request', 'response', 'info' tag found", str);
            //  throw new UnQualifiedXMLException();
            return null;
        }
        for (int i = 0; i < nodes.getLength(); i++) {
            String namespace = nodes.item(i).getNamespaceURI();
            String[] tmp_str = namespace.split(":");
            if(tmp_str.length!= 4){
                logger.error("XML namespace null or in malicious format ");
                return null;
            }
            if(!tmp_str[0].equals("urn") || !tmp_str[1].equals("cisco") || !tmp_str[2].equals("pnp")){
                logger.error("XML namespace {} in malicious format ", namespace);
                return null;
            }
            String service = tmp_str[tmp_str.length - 1];
            return service;
        }
        return null;
    }
    @Override
    public String xmlGenerator(Object obj) throws Exception {
        String result = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream original = new PrintStream(System.out);
            System.setOut(new PrintStream(baos));
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                    Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            XMLOutputFactory xof = XMLOutputFactory.newFactory();
            XMLStreamWriter xsw = xof.createXMLStreamWriter(baos, "UTF-8");
            xsw.writeStartDocument("UTF-8", "1.0");
            xsw = new MyXMLStreamWriter(xsw);
            marshaller.marshal(obj, xsw);
            xsw.close();
            result = baos.toString();
            baos.close();
            System.setOut(original);
            logger.info("Convert object to xml string : {}...", result);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return result;
    }

}
