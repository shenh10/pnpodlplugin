package org.opendaylight.controller.pnp.objects.api;


public interface PnpXmlHandler {
   public Object xmlParser(String string, String service) throws Exception;
   public String parseService(String str) throws Exception;
   public String xmlGenerator(Object obj) throws Exception;
}
