package org.opendaylight.controller.pnp.objects.assistant;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.classes.deviceInfoDir.Pnp;
import org.opendaylight.controller.pnp.objects.classes.deviceInfoDir.deviceInfo.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceInfoAssistant extends  AssistantParent{
    /**
     *
     */
    protected static final Logger logger = LoggerFactory
        .getLogger(DeviceInfoAssistant.class);
    private static final long serialVersionUID = 1L;
    protected List<FileSystem> filesystemList = null;
    protected HardwareInfo hardwareInfo = null;
    protected Udi udi = null;
    protected ImageInfo imageInfo;
    public void setFilesystemList( List<FileSystem> filesystemList){
        this.filesystemList = filesystemList;
    }
    public List<FileSystem> getFilesystemList(){
        return this.filesystemList;
    }
    public void setHardwareInfo(HardwareInfo hardwareInfo){
        this.hardwareInfo = hardwareInfo;
    }
    public HardwareInfo getHardwareInfo(){
        return this.hardwareInfo ;
    }
    public void setUdi(Udi udi){
        this.udi =udi;
    }
    public Udi getUdi(){
        return this.udi ;
    }
    public void setImageInfo(ImageInfo imageInfo){
        this.imageInfo =imageInfo;
    }
    public ImageInfo getImageInfo(){
        return this.imageInfo ;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((filesystemList == null) ? 0 : filesystemList.hashCode());
        result = prime * result + ((hardwareInfo == null) ? 0 : hardwareInfo.hashCode());
        result = prime * result + ((udi == null) ? 0 : udi.hashCode());
        result = prime * result + ((imageInfo == null) ? 0 : imageInfo.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DeviceInfoAssistant  other = (DeviceInfoAssistant) obj;
        if (filesystemList == null) {
            if (other.getFilesystemList() != null) {
                return false;
            }
        } else if (!filesystemList.equals(other.getFilesystemList())) {
            return false;
        }
        if (hardwareInfo == null) {
            if (other.getHardwareInfo() != null) {
                return false;
            }
        } else if (!hardwareInfo.equals(other.getHardwareInfo())) {
            return false;
        }
        if (imageInfo == null) {
            if (other.getImageInfo() != null) {
                return false;
            }
        } else if (!imageInfo.equals(other.getImageInfo())) {
            return false;
        }
        if (udi == null) {
            if (other.getUdi()!= null) {
                return false;
            }
        } else if (!udi.equals(other.getUdi())) {
            return false;
        }

        return true;
    }
    @Override
    public String toString(){
        return "[ filesystemList = :{" + filesystemList + "}, hardwareInfo= : {"
            + hardwareInfo + "}, udi = : {" + udi +"}, imageInfo =: { " +imageInfo+ "}]";
    }
    @Override
    public Object toFeature(){
        return null;
    }
    @Override
    public void fromFeature(Object obj){
        if(obj == null){
            logger.error("Pnp object give is null");
            return;
        }

        Pnp pnp = null;
        if(obj instanceof Pnp){
            pnp = (Pnp)obj;
        }else
        {
            logger.error("Given object is not Pnp instance.");
            return;
        }
        Response res = (Response)pnp.getRequestOrResponse();
        List<Serializable> list = res.getFileSystemListAndUdiAndHardwareInfo();
        for( int i = 0; i< list.size(); i++)
        {
            if(list.get(i) instanceof Response.FileSystemList){

                this.filesystemList = new ArrayList<FileSystem>();
                for(Response.FileSystemList.FileSystem fs:((Response.FileSystemList) list.get(i)).getFileSystem()){
                    FileSystem fs_tmp = new FileSystem();
                    fs_tmp.fromFeature(fs);
                    this.filesystemList.add(fs_tmp);
                }
            }
            if(list.get(i) instanceof Response.Udi){
                this.udi = new Udi();
                this.udi.fromFeature((Response.Udi)list.get(i));
            }
            if(list.get(i) instanceof Response.ImageInfo){
                this.imageInfo = new ImageInfo();
                this.imageInfo.fromFeature((Response.ImageInfo)list.get(i));
            }
            if(list.get(i) instanceof Response.HardwareInfo){
                this.hardwareInfo = new HardwareInfo();
                this.hardwareInfo.fromFeature((Response.HardwareInfo)list.get(i));
            }
        }
    }
    @Override
    public Object getErrorInfo(Object obj){
        Response res = (Response)obj;
        List<Serializable> list = res.getFileSystemListAndUdiAndHardwareInfo();
        for( int i = 0; i< list.size(); i++)
        {
            if(list.get(i) instanceof Response.ErrorInfo){
                return list.get(i);
            }
        }
        return null;
    }

    public static class FileSystem
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected boolean writeable;
            protected String type;
            protected int size;
            protected boolean readable;
            protected String name;
            protected int freespace;

            public boolean isWriteable() {
                return writeable;
            }

            public void setWriteable(boolean value) {
                this.writeable = value;
            }

            public String getType() {
                return type;
            }

            public void setType(String value) {
                this.type = value;
            }

            public int getSize() {
                return size;
            }

            public void setSize(int value) {
                this.size = value;
            }

            public boolean isReadable() {
                return readable;
            }

            public void setReadable(boolean value) {
                this.readable = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String value) {
                this.name = value;
            }

            public int getFreespace() {
                return freespace;
            }

            public void setFreespace(int value) {
                this.freespace = value;
            }
            @Override
            public int hashCode() {
                final int prime = 31;
                int result = 1;
                result= prime*result+ObjectUtils.hashCode(writeable);

                result= prime*result+((type == null) ? 0 : type.hashCode());

                result= prime*result+ObjectUtils.hashCode(size);

                result= prime*result+ObjectUtils.hashCode(readable);

                result= prime*result+((name == null) ? 0 : name.hashCode());

                result= prime*result+ObjectUtils.hashCode(freespace);

                return result;
            }
            @Override
            public boolean equals(Object obj){
                if (this == obj) {
                    return true;
                }
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false; } FileSystem other = (FileSystem) obj;
                if (writeable!= other.isWriteable()) {
                    return false;
                }

                if (type == null) {
                    if (other.getType() != null) {
                        return false;
                    }
                } else if (!type.equals(other.getType())) {
                    return false;
                }

                if ( size !=other.getSize()) {
                    return false;
                }

                if (readable!=other.isReadable()) {
                    return false;
                }

                if (name == null) {
                    if (other.getName() != null) {
                        return false;
                    }
                } else if (!name.equals(other.getName())) {
                    return false;
                }

                if (freespace != other.getFreespace()) {
                    return false;
                }

                return true;
            }

            @Override
            public String toString(){
                return "[ writable= :{" + writeable + "} type= :{" + type + "} size= :{" + size + "} readable= :{" + readable + "} name= :{" + name + "} freespace= :{" + freespace + "} }]"; }
            public void fromFeature(Object obj){
                if(obj == null)
                    return ;
                Response.FileSystemList.FileSystem obj_s = (Response.FileSystemList.FileSystem) obj;
                this.setWriteable(obj_s.isWriteable());


                this.setType(obj_s.getType());


                this.setSize(obj_s.getSize());


                this.setReadable(obj_s.isReadable());


                this.setName(obj_s.getName());


                this.setFreespace(obj_s.getFreespace());
            }

        }
    public static class HardwareInfo implements Serializable{

        private final static long serialVersionUID = 1L;
        protected String hostname;
        protected String vendor;
        protected String platformName;
        protected String processorType;
        protected String hwRevision;
        protected BigInteger mainMemSize;
        protected BigInteger ioMemSize;
        protected BigInteger boardId;
        protected String boardReworkId;
        protected String processorRev;
        protected String midplaneVersion;
        protected String location;

        public String getHostname() {
            return hostname;
        }

        public void setHostname(String value) {
            this.hostname = value;
        }

        public String getVendor() {
            return vendor;
        }

        public void setVendor(String value) {
            this.vendor = value;
        }

        public String getPlatformName() {
            return platformName;
        }

        public void setPlatformName(String value) {
            this.platformName = value;
        }

        public String getProcessorType() {
            return processorType;
        }

        public void setProcessorType(String value) {
            this.processorType = value;
        }

        public String getHwRevision() {
            return hwRevision;
        }

        public void setHwRevision(String value) {
            this.hwRevision = value;
        }

        public BigInteger getMainMemSize() {
            return mainMemSize;
        }

        public void setMainMemSize(BigInteger value) {
            this.mainMemSize = value;
        }

        public BigInteger getIoMemSize() {
            return ioMemSize;
        }

        public void setIoMemSize(BigInteger value) {
            this.ioMemSize = value;
        }

        public BigInteger getBoardId() {
            return boardId;
        }

        public void setBoardId(BigInteger value) {
            this.boardId = value;
        }

        public String getBoardReworkId() {
            return boardReworkId;
        }

        public void setBoardReworkId(String value) {
            this.boardReworkId = value;
        }

        public String getProcessorRev() {
            return processorRev;
        }

        public void setProcessorRev(String value) {
            this.processorRev = value;
        }

        public String getMidplaneVersion() {
            return midplaneVersion;
        }

        public void setMidplaneVersion(String value) {
            this.midplaneVersion = value;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String value) {
            this.location = value;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;

            result= prime*result+((hostname == null) ? 0 : hostname.hashCode());

            result= prime*result+((vendor == null) ? 0 : vendor.hashCode());

            result= prime*result+((platformName == null) ? 0 : platformName.hashCode());

            result= prime*result+((processorType == null) ? 0 : processorType.hashCode());

            result= prime*result+((hwRevision == null) ? 0 : hwRevision.hashCode());

            result= prime*result+((mainMemSize == null) ? 0 : mainMemSize.hashCode());

            result= prime*result+((ioMemSize == null) ? 0 : ioMemSize.hashCode());

            result= prime*result+((boardId == null) ? 0 : boardId.hashCode());

            result= prime*result+((boardReworkId == null) ? 0 : boardReworkId.hashCode());

            result= prime*result+((processorRev == null) ? 0 : processorRev.hashCode());

            result= prime*result+((midplaneVersion == null) ? 0 : midplaneVersion.hashCode());

            result= prime*result+((location == null) ? 0 : location.hashCode());

            return result;
        }
        @Override
        public boolean equals(Object obj){
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false; } HardwareInfo other = (HardwareInfo) obj;
            if (hostname == null) {
                if (other.getHostname() != null) {
                    return false;
                }
            } else if (!hostname.equals(other.getHostname())) {
                return false;
            }

            if (vendor == null) {
                if (other.getVendor() != null) {
                    return false;
                }
            } else if (!vendor.equals(other.getVendor())) {
                return false;
            }

            if (platformName == null) {
                if (other.getPlatformName() != null) {
                    return false;
                }
            } else if (!platformName.equals(other.getPlatformName())) {
                return false;
            }

            if (processorType == null) {
                if (other.getProcessorType() != null) {
                    return false;
                }
            } else if (!processorType.equals(other.getProcessorType())) {
                return false;
            }

            if (hwRevision == null) {
                if (other.getHwRevision() != null) {
                    return false;
                }
            } else if (!hwRevision.equals(other.getHwRevision())) {
                return false;
            }

            if (mainMemSize == null) {
                if (other.getMainMemSize() != null) {
                    return false;
                }
            } else if (!mainMemSize.equals(other.getMainMemSize())) {
                return false;
            }

            if (ioMemSize == null) {
                if (other.getIoMemSize() != null) {
                    return false;
                }
            } else if (!ioMemSize.equals(other.getIoMemSize())) {
                return false;
            }

            if (boardId == null) {
                if (other.getBoardId() != null) {
                    return false;
                }
            } else if (!boardId.equals(other.getBoardId())) {
                return false;
            }

            if (boardReworkId == null) {
                if (other.getBoardReworkId() != null) {
                    return false;
                }
            } else if (!boardReworkId.equals(other.getBoardReworkId())) {
                return false;
            }

            if (processorRev == null) {
                if (other.getProcessorRev() != null) {
                    return false;
                }
            } else if (!processorRev.equals(other.getProcessorRev())) {
                return false;
            }

            if (midplaneVersion == null) {
                if (other.getMidplaneVersion() != null) {
                    return false;
                }
            } else if (!midplaneVersion.equals(other.getMidplaneVersion())) {
                return false;
            }

            if (location == null) {
                if (other.getLocation() != null) {
                    return false;
                }
            } else if (!location.equals(other.getLocation())) {
                return false;
            }

            return true;
        }

        @Override
        public String toString(){
            return "[ hostname= :{" + hostname + "} vendor= :{" + vendor + "} platformName= :{" + platformName + "} processorType= :{" + processorType + "} hwRevision= :{" + hwRevision + "} mainMemSize= :{" + mainMemSize + "} ioMemSize= :{" + ioMemSize + "} boardId= :{" + boardId + "} boardReworkId= :{" + boardReworkId + "} processorRev= :{" + processorRev + "} midplaneVersion= :{" + midplaneVersion + "} location= :{" + location + "} }]"; }
        public void fromFeature(Object obj){
            if(obj== null)
                return ;
            Response.HardwareInfo obj_s = (Response.HardwareInfo)obj;

            this.setHostname(obj_s.getHostname());
            this.setVendor(obj_s.getVendor());
            this.setPlatformName(obj_s.getPlatformName());
            this.setProcessorType(obj_s.getProcessorType());
            this.setHwRevision(obj_s.getHwRevision());
            this.setMainMemSize(obj_s.getMainMemSize());
            this.setIoMemSize(obj_s.getIoMemSize());
            this.setBoardId(obj_s.getBoardId());
            this.setBoardReworkId(obj_s.getBoardReworkId());
            this.setProcessorRev(obj_s.getProcessorRev());
            this.setMidplaneVersion(obj_s.getMidplaneVersion());
            this.setLocation(obj_s.getLocation());

        }
    }
    public static class ImageInfo implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected String versionString;
        protected String imageFile;
        protected String imageHash;
        protected String returnToRomReason;
        protected String bootVariable;
        protected String bootLdrVariable;
        protected String configVariable;
        protected String configReg;
        protected String configRegNext;

        public String getVersionString() {
            return versionString;
        }
        public void setVersionString(String value) {
            this.versionString = value;
        }
        public String getImageFile() {
            return imageFile;
        }
        public void setImageFile(String value) {
            this.imageFile = value;
        }
        public String getImageHash() {
            return imageHash;
        }
        public void setImageHash(String value) {
            this.imageHash = value;
        }
        public String getReturnToRomReason() {
            return returnToRomReason;
        }
        public void setReturnToRomReason(String value) {
            this.returnToRomReason = value;
        }

        public String getBootVariable() {
            return bootVariable;
        }

        public void setBootVariable(String value) {
            this.bootVariable = value;
        }
        public String getBootLdrVariable() {
            return bootLdrVariable;
        }

        public void setBootLdrVariable(String value) {
            this.bootLdrVariable = value;
        }

        public String getConfigVariable() {
            return configVariable;
        }

        public void setConfigVariable(String value) {
            this.configVariable = value;
        }

        public String getConfigReg() {
            return configReg;
        }

        public void setConfigReg(String value) {
            this.configReg = value;
        }

        public String getConfigRegNext() {
            return configRegNext;
        }

        public void setConfigRegNext(String value) {
            this.configRegNext = value;
        }
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;

            result= prime*result+((versionString == null) ? 0 : versionString.hashCode());

            result= prime*result+((imageFile == null) ? 0 : imageFile.hashCode());

            result= prime*result+((imageHash == null) ? 0 : imageHash.hashCode());

            result= prime*result+((returnToRomReason == null) ? 0 : returnToRomReason.hashCode());

            result= prime*result+((bootVariable == null) ? 0 : bootVariable.hashCode());

            result= prime*result+((bootLdrVariable == null) ? 0 : bootLdrVariable.hashCode());

            result= prime*result+((configVariable == null) ? 0 : configVariable.hashCode());

            result= prime*result+((configReg == null) ? 0 : configReg.hashCode());

            result= prime*result+((configRegNext == null) ? 0 : configRegNext.hashCode());

            return result;
        }
        @Override
        public boolean equals(Object obj){
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false; } ImageInfo other = (ImageInfo) obj;
            if (versionString == null) {
                if (other.getVersionString() != null) {
                    return false;
                }
            } else if (!versionString.equals(other.getVersionString())) {
                return false;
            }

            if (imageFile == null) {
                if (other.getImageFile() != null) {
                    return false;
                }
            } else if (!imageFile.equals(other.getImageFile())) {
                return false;
            }
            if (imageHash == null) {
                if (other.getImageHash() != null) {
                    return false;
                }
            } else if (!imageHash.equals(other.getImageHash())) {
                return false;
            }

            if (returnToRomReason == null) {
                if (other.getReturnToRomReason() != null) {
                    return false;
                }
            } else if (!returnToRomReason.equals(other.getReturnToRomReason())) {
                return false;
            }

            if (bootVariable == null) {
                if (other.getBootVariable() != null) {
                    return false;
                }
            } else if (!bootVariable.equals(other.getBootVariable())) {
                return false;
            }
            if (bootLdrVariable == null) {
                if (other.getBootLdrVariable() != null) {
                    return false;
                }
            } else if (!bootLdrVariable.equals(other.getBootLdrVariable())) {
                return false;
            }

            if (configVariable == null) {
                if (other.getConfigVariable() != null) {
                    return false;
                }
            } else if (!configVariable.equals(other.getConfigVariable())) {
                return false;
            }

            if (configReg == null) {
                if (other.getConfigReg() != null) {
                    return false;
                }
            } else if (!configReg.equals(other.getConfigReg())) {
                return false;
            }

            if (configRegNext == null) {
                if (other.getConfigRegNext() != null) {
                    return false;
                }
            } else if (!configRegNext.equals(other.getConfigRegNext())) {
                return false;
            }

            return true;
        }

        @Override
        public String toString(){
            return "[ versionString= :{" + versionString + "} imageFile= :{" + imageFile + "} imageHash= :{" + imageHash + "} returnToRomReason= :{" + returnToRomReason + "} bootVariable= :{" + bootVariable + "} bootLdrVariable= :{" + bootLdrVariable + "} configVariable= :{" + configVariable + "} configReg= :{" + configReg + "} configRegNext= :{" + configRegNext + "} }]"; }
        public void fromFeature(Object obj){
            if(obj == null)
                return;
            Response.ImageInfo obj_s = (Response.ImageInfo)obj;
            this.setVersionString(obj_s.getVersionString());
            this.setImageFile(obj_s.getImageFile());
            this.setImageHash(obj_s.getImageHash());
            this.setReturnToRomReason(obj_s.getReturnToRomReason());
            this.setBootVariable(obj_s.getBootVariable());
            this.setBootLdrVariable(obj_s.getBootLdrVariable());
            this.setConfigVariable(obj_s.getConfigVariable());
            this.setConfigReg(obj_s.getConfigReg());
            this.setConfigRegNext(obj_s.getConfigRegNext());

        }
    }
    public static class Udi implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected String primaryChassis;
        protected StackedSwitch stackedSwitch;
        protected HaDevice haDevice;

        public String getPrimaryChassis() {
            return primaryChassis;
        }

        public void setPrimaryChassis(String value) {
            this.primaryChassis = value;
        }

        public StackedSwitch getStackedSwitch() {
            return stackedSwitch;
        }

        public void setStackedSwitch(StackedSwitch value) {
            this.stackedSwitch = value;
        }

        public HaDevice getHaDevice() {
            return haDevice;
        }

        public void setHaDevice(HaDevice value) {
            this.haDevice = value;
        }
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;

            result= prime*result+((primaryChassis == null) ? 0 : primaryChassis.hashCode());

            result= prime*result+((stackedSwitch == null) ? 0 : stackedSwitch.hashCode());

            result= prime*result+((haDevice == null) ? 0 : haDevice.hashCode());

            return result;
        }
        @Override
        public boolean equals(Object obj){
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false; } Udi other = (Udi) obj;
            if (primaryChassis == null) {
                if (other.getPrimaryChassis() != null) {
                    return false;
                }
            } else if (!primaryChassis.equals(other.getPrimaryChassis())) {
                return false;
            }
            if (stackedSwitch == null) {
                if (other.getStackedSwitch() != null) {
                    return false;
                }
            } else if (!stackedSwitch.equals(other.getStackedSwitch())) {
                return false;
            }

            if (haDevice == null) {
                if (other.getHaDevice() != null) {
                    return false;
                }
            } else if (!haDevice.equals(other.getHaDevice())) {
                return false;
            }

            return true;
        }

        @Override
        public String toString(){
            return "[ primaryChassis= :{" + primaryChassis + "} stackedSwitch= :{" + stackedSwitch + "} haDevice= :{" + haDevice + "} }]";
        }


        public static class HaDevice
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<String> standbyUdi;

                public List<String> getStandbyUdi() {
                    if (standbyUdi == null) {
                        standbyUdi = new ArrayList<String>();
                    }
                    return this.standbyUdi;
                }
                public void setStandbyUdi(List<String> list) {

                    this.standbyUdi = list;
                }
                @Override
                public int hashCode() {
                    final int prime = 31;
                    int result = 1;

                    result= prime*result+((standbyUdi == null) ? 0 : standbyUdi.hashCode());

                    return result;
                }
                @Override
                public boolean equals(Object obj){
                    if (this == obj) {
                        return true;
                    }
                    if (obj == null) {
                        return false;
                    }
                    if (getClass() != obj.getClass()) {
                        return false; } HaDevice other = (HaDevice) obj;
                    if (standbyUdi == null) {
                        if (other.getStandbyUdi() != null) {
                            return false;
                        }
                    } else if (!standbyUdi.equals(other.getStandbyUdi())) {
                        return false;
                    }

                    return true;
                }
                @Override
                public String toString(){
                    return "[ standbyUdi= :{" + standbyUdi + "} }]";
                }
                public void fromFeature(Object obj){
                    if(obj == null)
                        return;
                    Response.Udi.HaDevice obj_s =(Response.Udi.HaDevice) obj;
                    if(obj_s.getStandbyUdi() != null){
                        List<String> standbyUdi = new ArrayList<String>();
                        standbyUdi.addAll(obj_s.getStandbyUdi());
                        this.setStandbyUdi(standbyUdi);
                    }else
                        this.setStandbyUdi(null);
                }
            }
        public static class StackedSwitch
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<MemberUdi> memberUdi;


                public List<MemberUdi> getMemberUdi() {
                    if (memberUdi == null) {
                        memberUdi = new ArrayList<MemberUdi>();
                    }
                    return this.memberUdi;
                }
                public void setMemberUdi(List<MemberUdi> list) {

                    this.memberUdi = list;
                }
                public static class MemberUdi
                        implements Serializable
                    {

                        private final static long serialVersionUID = 1L;
                        protected String value;
                        protected BigInteger slot;


                        public String getValue() {
                            return value;
                        }
                        public void setValue(String value) {
                            this.value = value;
                        }
                        public BigInteger getSlot() {
                            return slot;
                        }
                        public void setSlot(BigInteger value) {
                            this.slot = value;
                        }
                        @Override
                        public int hashCode() {
                            final int prime = 31;
                            int result = 1;

                            result= prime*result+((value == null) ? 0 : value.hashCode());

                            result= prime*result+((slot == null) ? 0 : slot.hashCode());

                            return result;
                        }
                        @Override
                        public boolean equals(Object obj){
                            if (this == obj) {
                                return true;
                            }
                            if (obj == null) {
                                return false;
                            }
                            if (getClass() != obj.getClass()) {
                                return false; }
                            MemberUdi other = (MemberUdi) obj;
                            if (value == null) {
                                if (other.getValue() != null) {
                                    return false;
                                }
                            } else if (!value.equals(other.getValue())) {
                                return false;
                            }

                            if (slot == null) {
                                if (other.getSlot() != null) {
                                    return false;
                                }
                            } else if (!slot.equals(other.getSlot())) {
                                return false;
                            }

                            return true;
                        }

                        @Override
                        public String toString(){
                            return "[ value= :{" + value + "} slot= :{" + slot + "} }]";
                        }
                        public void fromFeature(Object obj){

                            if(obj == null)
                                return;
                            Response.Udi.StackedSwitch.MemberUdi obj_s = (Response.Udi.StackedSwitch.MemberUdi)obj;
                            this.slot = obj_s.getSlot();
                            this.value = obj_s.getValue();

                        }

                    }

                @Override
                public int hashCode() {
                    final int prime = 31;
                    int result = 1;

                    result= prime*result+((memberUdi == null) ? 0 : memberUdi.hashCode());

                    return result;
                }
                @Override
                public boolean equals(Object obj){
                    if (this == obj) {
                        return true;
                    }
                    if (obj == null) {
                        return false;
                    }
                    if (getClass() != obj.getClass()) {
                        return false; }
                    StackedSwitch other = (StackedSwitch) obj;
                    if (memberUdi == null) {
                        if (other.getMemberUdi() != null) {
                            return false;
                        }
                    } else if (!memberUdi.equals(other.getMemberUdi())) {
                        return false;
                    }

                    return true;
                }

                @Override
                public String toString(){
                    return "[ memberUdi= :{" + memberUdi + "} }]"; }
                public void fromFeature(Object obj){
                    if(obj == null)
                        return;
                    Response.Udi.StackedSwitch obj_s = (Response.Udi.StackedSwitch)obj;
                    if(obj_s.getMemberUdi() != null){
                        List<MemberUdi > memberUdi = new ArrayList<>();
                        for(Response.Udi.StackedSwitch.MemberUdi memUdi : obj_s.getMemberUdi() ){
                            MemberUdi memUdi_tmp = new MemberUdi();
                            memUdi_tmp.fromFeature(memUdi);
                            memberUdi.add(memUdi_tmp);
                        }
                        this.setMemberUdi(memberUdi);
                    }else
                        this.setMemberUdi(null);

                }
            }

        public void fromFeature(Object obj){
            Response.Udi obj_s = (Response.Udi) obj;
            if(obj_s.getHaDevice() != null){
                HaDevice haDevice = new HaDevice();
                haDevice.fromFeature(obj_s.getHaDevice());
                this.setHaDevice(haDevice);
            }else
                this.setHaDevice(null);

            this.setPrimaryChassis(obj_s.getPrimaryChassis());
            if(obj_s.getStackedSwitch() != null){
                StackedSwitch stackedSwitch = new StackedSwitch();
                stackedSwitch.fromFeature(obj_s.getStackedSwitch());
                this.setStackedSwitch(stackedSwitch);
            }else
                this.setStackedSwitch(null);
        }


    }
}
