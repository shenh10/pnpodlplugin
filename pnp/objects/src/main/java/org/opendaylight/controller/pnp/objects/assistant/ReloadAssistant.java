package org.opendaylight.controller.pnp.objects.assistant;

import java.lang.reflect.InvocationTargetException;

import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.classes.reloadDir.Pnp;
import org.opendaylight.controller.pnp.objects.classes.reloadDir.reload.ObjectFactory;
import org.opendaylight.controller.pnp.objects.classes.reloadDir.reload.Request;
import org.opendaylight.controller.pnp.objects.impl.CustomedConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReloadAssistant extends  AssistantParent{
    private final static long serialVersionUID = 1L;
    protected static final Logger logger = LoggerFactory
        .getLogger(ReloadAssistant.class);
    protected String reason;
    protected Integer delayIn;
    protected String user;
    protected Boolean configOper=true;
    protected Boolean isSaveConfig= null;
    protected String eraseConfig="";
    public void setReason(String value){

        this.reason = value;

    }

    public String getReason(){

        return this.reason;
    }
    public void setDelayIn(Integer value){

        this.delayIn = value;

    }

    public Integer getDelayIn(){

        return this.delayIn;
    }
    public void setUser(String value){

        this.user = value;

    }

    public String getUser(){

        return this.user;
    }
    public void setIsSaveConfig(Boolean value){

        this.isSaveConfig = value;

    }

    public Boolean getIsSaveConfig(){

        return this.isSaveConfig;
    }
    
    public void setConfigOper(Boolean value){

    	 this.configOper = value;

    	}

    	public Boolean getConfigOper(){

    	return this.configOper;
    	}
    	public void setEraseConfig(String value){

    	 this.eraseConfig = value;

    	}
    	public String getEraseConfig(){

    		return this.eraseConfig;
    		}


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result= prime*result+((reason == null) ? 0 : reason.hashCode());

        result= prime*result+((delayIn == null) ? 0 : delayIn.hashCode());

        result= prime*result+((user == null) ? 0 : user.hashCode());

        result= prime*result+((isSaveConfig == null) ? 0 : isSaveConfig.hashCode());

        result= prime*result+((configOper == null) ? 0 : configOper.hashCode());

        result= prime*result+((eraseConfig == null) ? 0 : eraseConfig.hashCode());

        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } ReloadAssistant other = (ReloadAssistant) obj;

        if (reason == null) {
            if (other.getReason() != null) {
                return false;
            }
        } else if (!reason.equals(other.getReason())) {
            return false;
        }

        if (delayIn == null) {
            if (other.getDelayIn() != null) {
                return false;
            }
        } else if (!delayIn.equals(other.getDelayIn())) {
            return false;
        }

        if (user == null) {
            if (other.getUser() != null) {
                return false;
            }
        } else if (!user.equals(other.getUser())) {
            return false;
        }

        if (isSaveConfig == null) {
            if (other.getIsSaveConfig() != null) {
                return false;
            }
        } else if (!isSaveConfig.equals(other.getIsSaveConfig())) {
            return false;
        }
        if (configOper == null) {
        	 if (other.getConfigOper() != null) {
        	 return false;
        	 }
        	 } else if (!configOper.equals(other.getConfigOper())) {
        	 return false;
        	 }

        	 if (eraseConfig == null) {
        	 if (other.getEraseConfig() != null) {
        	 return false;
        	 }
        	 } else if (!eraseConfig.equals(other.getEraseConfig())) {
        	 return false;
        	 }

        return true;
    }

    @Override
    public String toString(){
        return "[ reason= :{" + reason + "} delayIn= :{" + delayIn + "}user= :{" + user + "} isSaveConfig= :{" + isSaveConfig + "} configOper= :{" + configOper + "} eraseConfig= :{" + eraseConfig + "} }]"; }
    @Override
    public Object toFeature() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        CustomedConstructor constructor = new CustomedConstructor();
        Pnp obj = (Pnp)constructor.newInstance(Pnp.class);
        Request req = (Request) obj.getRequestOrResponse();
        if(req == null){
            req = (Request) constructor.newInstance(Request.class);
            if(req == null){
                return null;
            }
        }
        ObjectFactory objFactory = new ObjectFactory();
        if(objFactory == null){
            logger.error("Instance of {} allocate failure", ObjectFactory.class);
            return null;
        }
        Request.Reload reloadObj = objFactory.createRequestReload();
        reloadObj.setDelayIn(this.delayIn);
        reloadObj.setReason(this.reason);
        if (this.configOper == true){
        	if(this.isSaveConfig != null)
        		reloadObj.setSaveConfigOrEraseConfig(this.isSaveConfig);
        	else
        		reloadObj.setSaveConfigOrEraseConfig(this.eraseConfig);
        }
        reloadObj.setUser(this.user);
        
        req.setReload(reloadObj);
        return obj;
    }




}
