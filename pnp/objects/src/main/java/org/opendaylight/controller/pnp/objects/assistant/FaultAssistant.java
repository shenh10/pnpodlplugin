package org.opendaylight.controller.pnp.objects.assistant;

import javax.xml.bind.annotation.XmlElement;

import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.classes.faultDir.Pnp;
import org.opendaylight.controller.pnp.objects.classes.faultDir.fault.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FaultAssistant extends  AssistantParent{
    private final static long serialVersionUID = 1L;
    protected static final Logger logger = LoggerFactory
        .getLogger(FaultAssistant.class);
    protected String faultcode;
    protected String faultstring;
    protected String detail;
    public void setFaultcode(String value){

        this.faultcode = value;

    }

    public String getFaultcode(){

        return this.faultcode;
    }
    public void setFaultstring(String value){

        this.faultstring = value;

    }

    public String getFaultstring(){

        return this.faultstring;
    }
    public void setDetail(String value){

        this.detail = value;

    }

    public String getDetail(){

        return this.detail;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result= prime*result+((faultcode == null) ? 0 : faultcode.hashCode());

        result= prime*result+((faultstring == null) ? 0 : faultstring.hashCode());

        result= prime*result+((detail == null) ? 0 : detail.hashCode());

        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } FaultAssistant other = (FaultAssistant) obj;
        if (faultcode == null) {
            if (other.getFaultcode() != null) {
                return false;
            }
        } else if (!faultcode.equals(other.getFaultcode())) {
            return false;
        }

        if (faultstring == null) {
            if (other.getFaultstring() != null) {
                return false;
            }
        } else if (!faultstring.equals(other.getFaultstring())) {
            return false;
        }

        if (detail == null) {
            if (other.getDetail() != null) {
                return false;
            }
        } else if (!detail.equals(other.getDetail())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString(){
        return "[ faultcode= :{" + faultcode + "} faultstring= :{" + faultstring + "} detail= :{" + detail + "} }]"; }
    @Override
    public void fromFeature(Object obj){
        if(obj == null){
            logger.error("Pnp object given is null");
            return;
        }

        Pnp pnp = null;
        if(obj instanceof Pnp){
            pnp = (Pnp)obj;
        }else
        {
            logger.error("Given object is not Pnp instance.");
            return;
        }
        Response res =  pnp.getResponse();
        if(res == null){
            logger.error("No response body");
            return ;
        }

        Response.Fault fault = res.getFault();
        if(fault == null){
            logger.error("Fault Object is null");
            return;
        }
        this.setDetail(fault.getDetail());
        this.setFaultcode(fault.getFaultcode());
        this.setFaultstring(fault.getFaultstring());
    }

}
