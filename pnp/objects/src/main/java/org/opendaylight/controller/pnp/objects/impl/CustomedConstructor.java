package org.opendaylight.controller.pnp.objects.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

import org.opendaylight.controller.pnp.objects.api.ICustomedConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomedConstructor implements ICustomedConstructor{
	protected static final Logger logger = LoggerFactory
	        .getLogger(CustomedConstructor.class);
	
	@Override
    public Object newInstance(Class<?> cls) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        Object root = cls.newInstance();
        Field[] fields = cls.getDeclaredFields();
        for (Field field: fields){
            Annotation[] annos = field.getAnnotations();
            for(Annotation anno : annos){
                if(anno instanceof XmlElements){
                    XmlElement[] eles = ((XmlElements) anno).value();
                    for(XmlElement ele: eles){
                        if(ele.name().equals("request")){
                            Object obj = newInstance(ele.type());
                            String methodName = "";
                            for(int i = 0; i< eles.length; i++){
                                methodName += firstCharToUpper(eles[i].name());
                                if(i< eles.length-1)
                                    methodName += "Or";
                            }
                            root.getClass().getMethod("set"+methodName, field.getType()).invoke(root, obj);
                        }
                    }
                }
                if(anno instanceof XmlAttribute){
                    if(((XmlAttribute) anno).required()){
                        Object obj = field.getType().newInstance();
                        String methodName = firstCharToUpper(((XmlAttribute) anno).name());
                        root.getClass().getMethod("set"+methodName, field.getType()).invoke(root,obj);
                    }
                }
                if(anno instanceof XmlElement){
                    if(((XmlElement) anno).required()){
                        Object obj = null;
                        if(field.getType().equals(Object.class))
                            obj = "";
                        else obj = newInstance(field.getType());
                        String methodName = firstCharToUpper(field.getName());
                        root.getClass().getMethod("set"+methodName, field.getType()).invoke(root,obj);
                    }
                }
            }
        }
        if(root == null){
        	logger.error("Instance of {} allocation failure", cls);
        }
        	
        logger.debug("Use customized constructor to generate {}", root);
        return root;
    }


    public String firstCharToUpper(String str){
        String r_str = str.substring(0,1).toUpperCase()+str.substring(1);
        return r_str;
    }

}
