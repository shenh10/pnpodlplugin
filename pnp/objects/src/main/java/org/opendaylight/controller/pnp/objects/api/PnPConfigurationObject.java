package org.opendaylight.controller.pnp.objects.api;

import org.opendaylight.controller.configuration.ConfigurationObject;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public class PnPConfigurationObject extends ConfigurationObject{
    private final static long serialVersionUID = 1L;
}
