package org.opendaylight.controller.pnp.objects.api;

import java.util.Map;

public interface PnpServiceList {
    public void populate();
    public Class<?> getService(String str);
    public Boolean containsService(String str);
    public Map<String, Class<?>> getPnPDict();
    public Class<?> getAssistant(String str);
    public Map<String, Class<?>> getPnPAssistantDict();
}
