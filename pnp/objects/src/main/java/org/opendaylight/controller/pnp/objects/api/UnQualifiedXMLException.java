package org.opendaylight.controller.pnp.objects.api;

public class UnQualifiedXMLException extends Exception{

       public UnQualifiedXMLException() {
           // TODO Auto-generated constructor stub
       }

       public UnQualifiedXMLException(String message) {
           super(message);
           // TODO Auto-generated constructor stub
       }

       public UnQualifiedXMLException(Throwable cause) {
           super(cause);
           // TODO Auto-generated constructor stub
       }

       public UnQualifiedXMLException(String message, Throwable cause) {
           super(message, cause);
           // TODO Auto-generated constructor stub
       }
}
