package org.opendaylight.controller.pnp.objects.impl;

import java.lang.reflect.InvocationTargetException;

import org.junit.Assert;
import org.junit.Test;

public class CustomedConstructorTest {
	@Test
	   public void testNewInstance() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		CustomedConstructor constructor = new CustomedConstructor();
		Object obj = constructor.newInstance(org.opendaylight.controller.pnp.objects.classes.imageInstallDir.Pnp.class);
		Assert.assertEquals(org.opendaylight.controller.pnp.objects.classes.imageInstallDir.Pnp.class, obj.getClass());
	}
}
