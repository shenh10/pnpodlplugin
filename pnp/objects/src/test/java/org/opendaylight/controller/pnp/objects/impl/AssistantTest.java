package org.opendaylight.controller.pnp.objects.impl;

import java.lang.reflect.InvocationTargetException;

import junit.framework.Assert;

import org.junit.Test;
import org.opendaylight.controller.pnp.objects.assistant.BackoffAssistant;
import org.opendaylight.controller.pnp.objects.assistant.ConfigUpgradeAssistant;
import org.opendaylight.controller.pnp.objects.assistant.DeviceInfoAssistant;
import org.opendaylight.controller.pnp.objects.assistant.ImageInstallAssistant;
import org.opendaylight.controller.pnp.objects.assistant.ReloadAssistant;

public class AssistantTest {
     @Test
     public void testImageInstall() throws Exception{
    	 String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp xmlns=\"urn:cisco:pnp\" version=\"\" udi=\"\" noCheckTime=\"19\"><request xmlns=\"urn:cisco:pnp:image-install\" correlator=\"\"><image><copy><source><location>http://pnp.me/src/image</location></source></copy></image><noReload></noReload></request></pnp>";
    	 ImageInstallAssistant assis = new ImageInstallAssistant();
    	 assis.setSrcLocation("http://pnp.me/src/image");
    	 assis.setNoCheckTime(19);
    	 assis.setIsReload(false);
    	 Object feature = assis.toFeature();
    	 XMLHandler xmlhandler = new XMLHandler();
    	 Assert.assertEquals(str, xmlhandler.xmlGenerator(feature));  	 
     }
     @SuppressWarnings("deprecation")
	@Test
     public void testConfigUpgrade() throws Exception{
    	 String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp xmlns=\"urn:cisco:pnp\" version=\"\" udi=\"\" noCheckTime=\"19\"><request xmlns=\"urn:cisco:pnp:config-upgrade\" correlator=\"\"><config details=\"brief\"><copy><source><location>tftp://172.19.211.47//tftpboot/rouppala/test_cfg</location></source><applyTo>startup</applyTo><abortOnSyntaxFault></abortOnSyntaxFault></copy></config><noReload></noReload></request></pnp>";
    	 ConfigUpgradeAssistant assis = new ConfigUpgradeAssistant();
    	 assis.setSrcLocation("tftp://172.19.211.47//tftpboot/rouppala/test_cfg");
    	 assis.setNoCheckTime(19);
    	 assis.setIsReload(false);
    	 assis.setDetails("brief");
    	 assis.setApplyTo("startup");
    	 Object feature = assis.toFeature();
    	 XMLHandler xmlhandler = new XMLHandler();
    	 Assert.assertEquals(str, xmlhandler.xmlGenerator(feature));
     }
     @Test
     public void testBackoff() throws Exception{
    	 String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp xmlns=\"urn:cisco:pnp\" version=\"\" udi=\"\"><request xmlns=\"urn:cisco:pnp:backoff\" correlator=\"\"><backoff><reason>no reason</reason><terminate>terminates!</terminate></backoff></request></pnp>";
    	 BackoffAssistant assis = new BackoffAssistant();
    	 assis.setReason("no reason");
    	 assis.setTerminate("terminates!");
         assis.setBfChoice(0);
    	 Object feature = assis.toFeature();
    	 XMLHandler xmlhandler = new XMLHandler();
    	 Assert.assertEquals(str, xmlhandler.xmlGenerator(feature));
    	 
     }
	@Test
     public void testDeviceInfo() throws Exception{
    	 String str="<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp version=\"1.0\" udi=\"PID:7206VXR,VID:,SN:34835437\" usr=\"usr15\" pwd=\"pwd15\" xmlns=\"urn:cisco:pnp\"><response xmlns=\"urn:cisco:pnp:device-info\" correlator=\"devinfo20140225155136760\" version=\"1.0\" success=\"1\"><fileSystemList><fileSystem writeable=\"1\"  type=\"flash\" size=\"57671680\" readable=\"1\" freespace=\"17323520\" name=\"flash\" /><fileSystem writeable=\"1\"  type=\"flash\" size=\"6838272\" readable=\"1\" freespace=\"6836224\" name=\"fstage\"     /><fileSystem writeable=\"1\"  type=\"nvram\" size=\"524288\" readable=\"1\" freespace=\"512589\" name=\"nvram\" /></fileSystemList><udi><primary-chassis>lalala</primary-chassis><stacked-switch><member-udi slot=\"0\" >lalala</member-udi></stacked-switch></udi><hardwareInfo><hostname> C3750X-1</hostname><vendor> cisco</vendor><platformName> WS-C3750X-24</platformName><processorType> PowerPC405</processorType><hwRevision> A0</hwRevision><mainMemSize> 268435456</mainMemSize><ioMemSize> 0</ioMemSize><boardId> FDO1703P2EB</boardId><boardReworkId/><processorRev/><midplaneVersion/><location>San Jose</location></hardwareInfo><imageInfo><versionString>Cisco IOS Software, C3750E Software (C3750E-UNIVERSALK9-M), Experimental Version 15.2(20131111:213707) [pmamidi-dsgs_nonvlan1 112] Copyright (c) 1986-2013 by Cisco Systems, Inc.Compiled Fri 15-Nov-13 11:36 by pmamidi</versionString><imageFile>flash:c3750e-universalk9-mz</imageFile><imageHash/><returnToRomReason>power-on</returnToRomReason><bootVariable/><bootLdrVariable/><configVariable/><configReg>0xF</configReg><configRegNext/></imageInfo></response></pnp>";
    	 XMLHandler xmlHandler = new XMLHandler();
    	 String service = xmlHandler.parseService(str);
    	 Assert.assertEquals("device-info", service);
    	 Object pnp = xmlHandler.xmlParser(str, service);
    	 DeviceInfoAssistant assis = new DeviceInfoAssistant();
    	 assis.fromFeature(pnp);
    	 Assert.assertNotNull(assis);
     }
	@Test
	   public void testReload() throws Exception{
		   	String str ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp xmlns=\"urn:cisco:pnp\" version=\"\" udi=\"\"><request xmlns=\"urn:cisco:pnp:reload\" correlator=\"\"><reload><reason>none</reason><delayIn>30</delayIn><user>jpaul</user><saveConfig>true</saveConfig></reload></request></pnp>";
		   	XMLHandler xmlHandler = new XMLHandler();
	    	 String service = xmlHandler.parseService(str);
	    	 Assert.assertEquals("reload", service);
	    	 Object pnp = xmlHandler.xmlParser(str, service);
	    	 ReloadAssistant assis = new ReloadAssistant();
	    	 assis.setDelayIn(30);
	    	 assis.setConfigOper(true);
	    	 assis.setIsSaveConfig(true);
	    	 assis.setReason("none");
	    	 assis.setUser("jpaul");
	    	 Assert.assertEquals(str, xmlHandler.xmlGenerator(assis.toFeature()));
	     }
	@Test
	   public void testFault() throws Exception{
		   	String str ="<pnp xmlns=\"urn:cisco:pnp\" version=\"\" udi=\"\"><response correlator=\"\" xmlns=\"urn:cisco:pnp:fault\"><fault><faultcode>No attribute</faultcode><faultstring>Attribute</faultstring><detail>No attribute</detail></fault></response></pnp>";
	    	 XMLHandler xmlHandler = new XMLHandler();
	    	 String service = xmlHandler.parseService(str);
	    	 Assert.assertEquals("fault", service);
	    	 Object pnp = xmlHandler.xmlParser(str, service);
	    	 DeviceInfoAssistant assis = new DeviceInfoAssistant();
	    	 assis.fromFeature(pnp);
	    	 Assert.assertNotNull(assis);
	     }
	
}
