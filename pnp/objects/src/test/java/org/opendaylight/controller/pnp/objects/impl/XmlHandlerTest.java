package org.opendaylight.controller.pnp.objects.impl;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.opendaylight.controller.pnp.objects.classes.workInfoDir.Pnp;
import org.xml.sax.SAXException;

public class XmlHandlerTest {
   @Rule
   public ExpectedException thrown= ExpectedException.none();

   @Test
   public void testXmlParserWithLegalInput() throws Exception {
          String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp xmlns=\"urn:cisco:pnp\" pid=\"pid78675645\"><request xmlns=\"urn:cisco:pnp:device-id\" correlator=\"434234131\" version=\"1.0\"><deviceId><udi>Pid:Pid7868767,Vid:,SN:y7867</udi></deviceId></request></pnp>";
          XMLHandler xmlhandler = new XMLHandler();
          Assert.assertNotNull(xmlhandler);
          String service = xmlhandler.parseService(str);
          Assert.assertEquals("device-id",service);
   }
   @Test
   public void testXmlParserWithNoneXMLInput() throws Exception {
          String str = "123";
          XMLHandler xmlhandler = new XMLHandler();
          Assert.assertNotNull(xmlhandler);
          thrown.expect(SAXException.class);
          String service = xmlhandler.parseService(str);
   }
   @Test
   public void testXmlParserWithOtherXMLInput() throws Exception {
          String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><load></load>";
          XMLHandler xmlhandler = new XMLHandler();
          Assert.assertNotNull(xmlhandler);
          String service = xmlhandler.parseService(str);
          Assert.assertNull(service);
   }
   @Test
   public void testXmlParserAndGenerator() throws Exception{
         String str = "<pnp xmlns=\"urn:cisco:pnp\" version=\"1.0\" udi=\"PID:7206VXR,VID:,SN:34835437\" usr=\"usr15\" pwd=\"pwd15\"><info xmlns=\"urn:cisco:pnp:work-info\"     correlator=\"udi18\"><deviceId><udi>PID: ISCO7206VXR,VID:,SN:34835437</udi><authRequired>true</authRequired></deviceId></info></pnp>";
          XMLHandler xmlhandler = new XMLHandler();
          Assert.assertNotNull(xmlhandler);
          String service = xmlhandler.parseService(str);
          Assert.assertEquals("work-info",service);
          service = "device-id";
          Object obj = xmlhandler.xmlParser(str, service);
          Assert.assertNotNull(obj);
          String res = xmlhandler.xmlGenerator(obj);
          Assert.assertNotNull(res);
   }
//   @Test
//   public void testWorkInfoResponseParser() throws Exception{
//           CustomedConstructor constructor = new CustomedConstructor();
//           Pnp workInfo =  (Pnp) constructor.newInstance(Pnp.class);
//           XMLHandler xmlhandler = new XMLHandler();
//           Assert.assertNotNull(xmlhandler);
//           System.out.println(xmlhandler.xmlGenerator(workInfo));
//   }
//   @Test
//   public void testXmlParserWithIllegalXMLInput() throws Exception{
//      String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><pnp xmlns=\"urn:cisco:pnp\" pid=\"pid78675645\"><request xmlns=\"urn:cisco:pnp:device-id\"  version=\"1.0\"></request></pnp>";
//          XMLHandler xmlhandler = new XMLHandler();
//          Assert.assertNotNull(xmlhandler);
//          String service = xmlhandler.parseService(str);
//          Assert.assertEquals("device-id",service);
//          Object obj = xmlhandler.xmlParser(str, service);
//          Assert.assertNotNull(obj);
//   }
}
