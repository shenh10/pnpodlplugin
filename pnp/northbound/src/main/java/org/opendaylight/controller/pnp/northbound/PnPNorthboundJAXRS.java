package org.opendaylight.controller.pnp.northbound;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
//import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;
//import javax.ws.rs.ext.ContextResolver;


import org.codehaus.enunciate.jaxrs.ResponseCode;
import org.codehaus.enunciate.jaxrs.StatusCodes;
//import org.opendaylight.controller.northbound.commons.query.QueryContext;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.storage.api.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/")
public class PnPNorthboundJAXRS {
    protected static final Logger logger = LoggerFactory
        .getLogger(PnPNorthboundJAXRS.class);
    protected PnPNorthboundImpl impl = new PnPNorthboundImpl();
    //    void setPnPNorthboundImpl(PnPNorthboundImpl impl){
    //        this.impl = impl;
    //        logger.trace("Got PnPNorthboundImpl  set request {}", impl);
    //
    //    }
    //    void unsetPnPNorthboundImpl(){
    //        this.impl = null;
    //        logger.trace("Got  PnPNorthboundImpl service UNset request");
    //    }
    //    private String username;
    //    private QueryContext queryContext;
    //
    //    @Context
    //    public void setQueryContext(ContextResolver<QueryContext> queryCtxResolver) {
    //      if (queryCtxResolver != null) {
    //        queryContext = queryCtxResolver.getContext(QueryContext.class);
    //      }
    //    }
    //
    //    @Context
    //    public void setSecurityContext(SecurityContext context) {
    //        if (context != null && context.getUserPrincipal() != null) {
    //            username = context.getUserPrincipal().getName();
    //        }
    //    }
    //
    //    protected String getUserName() {
    //        return username;
    //    }
    @Path("load/{table}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public List<Object> loadTable(@PathParam("table") String table){
        return impl.loadTable(table);
    }
    @Path("load/master/{udi}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public MasterEntry loadDeviceEntry(@PathParam("udi") String udi){
        return impl.loadDeviceEntry(udi);
    }

    @Path("device/add")
    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response addDevice( MasterEntry entry){
        if(impl.addDevice(entry))
            return Response.ok().build();
        else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
    @Path("device/del/{udi}")
    @GET
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public Response delDevice(@PathParam("udi") String udi){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        if(impl.deleteDevice(udi))
            return Response.ok().build();
        else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
    @Path("device/update")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public Response delDevice(MasterEntry entry){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        if(impl.updateDevice(entry))
            return Response.ok().build();
        else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path("feature/update/day1/{udi}")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public Response updateDayOneFeature(@PathParam("udi")String udi, String content){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        JSONConverter converter = new JSONConverter();
        if(impl.updateFeature(udi, (List<Pair<String, AssistantParent>>) converter.featureListJsonToObject(content),0))
            return Response.ok().build();
        else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();

    }

    @Path("feature/update/day2/{udi}")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public Response updateDayTwoFeature(@PathParam("udi")String udi, String content){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        JSONConverter converter = new JSONConverter();
        if(impl.updateFeature(udi, (List<Pair<String, AssistantParent>>) converter.featureListJsonToObject(content),1))
            return Response.ok().build();
        else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();

    }

    @Path("feature/load/day1/{udi}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public List<Pair<String, AssistantParent>> loadDayOneFeature(@PathParam("udi")String udi){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        return impl.loadFeatures(udi,0);
    }
    @Path("feature/load/day2wait/{udi}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public List<Pair<String, AssistantParent>> loadDayTwoWaitFeature(@PathParam("udi")String udi){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        return impl.loadFeatures(udi,1);
    }

    @Path("feature/load/day2done/{udi}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public List<Pair<String, AssistantParent>> loadDayTwoDonwFeature(@PathParam("udi")String udi){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        return impl.loadFeatures(udi,2);
    }
    @Path("load/fault/day1/{udi}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public AssistantParent loadDayOneFault(@PathParam("udi")String udi){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        return impl.loadFault(udi,0);
    }
    @Path("load/fault/day2/{udi}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public AssistantParent loadDayTwoFault(@PathParam("udi")String udi){
        //   PnPNorthboundImpl impl= new PnPNorthboundImpl();
        return impl.loadFault(udi,1);
    }

    @Path("deviceInfo/load/{udi}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @StatusCodes({ @ResponseCode(code = 404, condition = "The Container Name was not found") })
    public AssistantParent loadDeviceInfo(@PathParam("udi")String udi){
        //    PnPNorthboundImpl impl= new PnPNorthboundImpl();
        return impl.loadDeviceInfo(udi);
    }
    @Path("device/reset/{udi}")
    @GET
    public Response resetStatus(@PathParam("udi") String udi){
        if(impl.resetStatus(udi)){
            return Response.ok().build();
        }else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
    @Path("device/claim/{udi}")
    @GET
    public Response claimDevice(@PathParam("udi") String udi){
        if(impl.claimDevice(udi)){
            return Response.ok().build();
        }else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
    @Path("device/reload/{udi}")
    @GET
    public Response reloadDevice(@PathParam("udi") String udi) throws InstantiationException, IllegalAccessException{
        if(impl.reloadDevice(udi)){
            return Response.ok().build();
        }else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

}
