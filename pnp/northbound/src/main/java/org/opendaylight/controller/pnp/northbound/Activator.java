package org.opendaylight.controller.pnp.northbound;

import org.apache.felix.dm.Component;
import org.opendaylight.controller.sal.core.ComponentActivatorAbstractBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Activator extends ComponentActivatorAbstractBase{
    protected static final Logger logger = LoggerFactory
        .getLogger(Activator.class);
    @Override
    public Object[] getImplementations() {
        return new Object[]{};
    }
    @Override
    public void configureInstance(Component c, Object imp, String containerName) {
    }
    @Override
    protected Object[] getGlobalImplementations() {
        Object[] res = {PnPNorthboundJAXRS.class, PnPNorthboundImpl.class};
        return res;
    }

    @Override
    protected void configureGlobalInstance(Component c, Object imp) {
        if (imp.equals(PnPNorthboundJAXRS.class)) {
//            c.add(createServiceDependency().setService(
//                    PnPNorthboundImpl.class).setCallbacks(
//                    "setPnPNorthboundImpl",
//                    "unsetPnPNorthboundImpl").setRequired(true));
        }
        if (imp.equals(PnPNorthboundImpl.class)) {
             c.setInterface(new String[] {
                     PnPNorthboundImpl.class.getName()}, null);
        }
    }
}

