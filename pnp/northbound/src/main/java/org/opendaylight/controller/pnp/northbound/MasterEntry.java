package org.opendaylight.controller.pnp.northbound;

import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.storage.api.DeviceKey;
import org.opendaylight.controller.pnp.storage.api.IMasterDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MasterEntry extends AssistantParent{
    /**
     *
     */
    private static final Logger log = LoggerFactory.getLogger(MasterEntry.class);
    private static final long serialVersionUID = 1L;
    protected String udi = null;
    protected String deviceName = null;
    protected Boolean claimedFlag = false;
    protected String location = null;
    protected String errorCode = null;
    protected String errorMessage = null;
    protected String errorSeverity = null;
    protected Boolean faultFlag = false;
    protected String status;
    protected String dynamicStatus;
    protected String usr;
    protected String pwd;

    public void setUdi(String value){

        this.udi = value;

    }

    public String getUdi(){

        return this.udi;
    }
    public void setDeviceName(String value){

        this.deviceName = value;

    }

    public String getDeviceName(){

        return this.deviceName;
    }
    public void setLocation(String value){

        this.location = value;

    }

    public String getLocation(){

        return this.location;
    }
    public void setErrorCode(String value){

        this.errorCode = value;

    }

    public String getErrorCode(){

        return this.errorCode;
    }
    public void setErrorMessage(String value){

        this.errorMessage = value;

    }

    public String getErrorMessage(){

        return this.errorMessage;
    }
    public void setErrorSeverity(String value){

        this.errorSeverity = value;

    }

    public String getErrorSeverity(){

        return this.errorSeverity;
    }
    public void setStatus(String value){

        this.status = value;

    }

    public String getStatus(){

        return this.status;
    }
    public void setClaimedFlag(Boolean value){

        this.claimedFlag = value;

    }

    public Boolean getClaimedFlag(){

        return this.claimedFlag;
    }
    public void setUsr(String value){

        this.usr = value;

    }

    public String getUsr(){

        return this.usr;
    }
    public void setPwd(String value){

        this.pwd = value;

    }
    public String getPwd(){

        return this.pwd;
    }

    public void setDynamicStatus(String value){

        this.dynamicStatus = value;

    }

    public String getDynamicStatus(){

        return this.dynamicStatus;
    }
    public void setFaultFlag(Boolean value){

        this.faultFlag = value;

    }

    public Boolean getFaultFlag(){

        return this.faultFlag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result= prime*result+((udi == null) ? 0 : udi.hashCode());

        result= prime*result+((deviceName == null) ? 0 : deviceName.hashCode());

        result= prime*result+((location == null) ? 0 : location.hashCode());

        result= prime*result+((errorCode == null) ? 0 : errorCode.hashCode());

        result= prime*result+((errorMessage == null) ? 0 : errorMessage.hashCode());

        result= prime*result+((errorSeverity == null) ? 0 : errorSeverity.hashCode());

        result= prime*result+((status == null) ? 0 : status.hashCode());

        result= prime*result+((claimedFlag == null) ? 0 : claimedFlag.hashCode());

        result= prime*result+((usr == null) ? 0 : usr.hashCode());

        result= prime*result+((pwd == null) ? 0 : pwd.hashCode());

        result= prime*result+((dynamicStatus == null) ? 0 : dynamicStatus.hashCode());

        result= prime*result+((faultFlag == null) ? 0 : faultFlag.hashCode());

        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } MasterEntry other = (MasterEntry) obj;
        if (udi == null) {
            if (other.getUdi() != null) {
                return false;
            }
        } else if (!udi.equals(other.getUdi())) {
            return false;
        }

        if (deviceName == null) {
            if (other.getDeviceName() != null) {
                return false;
            }
        } else if (!deviceName.equals(other.getDeviceName())) {
            return false;
        }

        if (location == null) {
            if (other.getLocation() != null) {
                return false;
            }
        } else if (!location.equals(other.getLocation())) {
            return false;
        }

        if (errorCode == null) {
            if (other.getErrorCode() != null) {
                return false;
            }
        } else if (!errorCode.equals(other.getErrorCode())) {
            return false;
        }

        if (errorMessage == null) {
            if (other.getErrorMessage() != null) {
                return false;
            }
        } else if (!errorMessage.equals(other.getErrorMessage())) {
            return false;
        }

        if (errorSeverity == null) {
            if (other.getErrorSeverity() != null) {
                return false;
            }
        } else if (!errorSeverity.equals(other.getErrorSeverity())) {
            return false;
        }

        if (status == null) {
            if (other.getStatus() != null) {
                return false;
            }
        } else if (!status.equals(other.getStatus())) {
            return false;
        }
        if (claimedFlag == null) {
            if (other.getClaimedFlag() != null) {
                return false;
            }
        } else if (!claimedFlag.equals(other.getClaimedFlag())) {
            return false;
        }
        if (usr == null) {
            if (other.getUsr() != null) {
                return false;
            }
        } else if (!usr.equals(other.getUsr())) {
            return false;
        }

        if (pwd == null) {
            if (other.getPwd() != null) {
                return false;
            }
        } else if (!pwd.equals(other.getPwd())) {
            return false;
        }
        if (dynamicStatus == null) {
            if (other.getDynamicStatus() != null) {
                return false;
            }
        } else if (!dynamicStatus.equals(other.getDynamicStatus())) {
            return false;
        }

        if (faultFlag == null) {
            if (other.getFaultFlag() != null) {
                return false;
            }
        } else if (!faultFlag.equals(other.getFaultFlag())) {
            return false;
        }


        return true;
    }

    @Override
    public String toString(){
        return "[ udi= :{" + udi + "} deviceName= :{" + deviceName + "} location= :{" + location
            + "} errorCode= :{" + errorCode + "} errorMessage= :{" + errorMessage
            + "} errorSeverity= :{" + errorSeverity + "} status= :{"
            + status + "} claimedFlag= :{" + claimedFlag  + "} usr = :{"+usr + "} pwd = :{"+pwd
            + "} dynamicStatus= :{" + dynamicStatus + "} faultFlag= :{" + faultFlag + "} ]"; }

    public Object toDBEntry(){
        IMasterDB entry = new IMasterDB();
        if(udi !=null){
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                log.error("udi {} not in proper format! ", udi);
                return null;
            }
            entry.setDkey(dkey);
        }
        if(deviceName !=null){

            entry.setDeviceName(deviceName);
        }
        if(location !=null){

            entry.setLocation(location);
        }
        if(usr != null){
            entry.setUsr(usr);
        }
        if(pwd != null){
            entry.setPwd(usr);
        }
        return entry;
    }
    public void updateDBEntry(IMasterDB entry){

        if(udi !=null){
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                log.error("udi {} not in proper format! ", udi);
                return ;
            }
            entry.setDkey(dkey);
        }
        if(deviceName !=null){

            entry.setDeviceName(deviceName);
        }
        if(location !=null){

            entry.setLocation(location);
        }
        if(usr != null){
            entry.setUsr(usr);
        }
        if(pwd != null){
            entry.setPwd(usr);
        }

    }
    public void fromDBEntry(IMasterDB entry){
        this.deviceName = entry.getDeviceName();
        this.location = entry.getLocation();
        this.status = entry.getStatus().getStatus();
        this.errorCode = entry.getErrorCode();
        this.errorMessage = entry.getErrorMessage();
        this.errorSeverity = entry.getErrorSeverity();
        this.udi = entry.getDkey().toString();
        this.claimedFlag = entry.getClaimedFlag();
        this.usr = entry.getUsr();
        this.pwd = entry.getPwd();
        this.dynamicStatus = entry.getDynamicStatus().getStatus();
        this.faultFlag = entry.getFaultFlag();
    }
}
