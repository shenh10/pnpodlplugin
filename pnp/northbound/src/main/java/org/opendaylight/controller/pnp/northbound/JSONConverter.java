package org.opendaylight.controller.pnp.northbound;

import java.util.ArrayList;
import java.util.List;

import org.opendaylight.controller.northbound.commons.RestMessages;
import org.opendaylight.controller.northbound.commons.exception.ResourceNotFoundException;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.storage.api.Pair;
import org.opendaylight.controller.sal.utils.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import  com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
public class JSONConverter {
    protected static final Logger logger = LoggerFactory
        .getLogger(JSONConverter.class);
    private Gson gson ;
    private PnpServiceList services;
    public JSONConverter(){
        gson = new Gson();
        this.services = (PnpServiceList) ServiceHelper.getGlobalInstance(PnpServiceList.class,
                this);
        if (this.services == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
    }
    public String objToJson(Object obj){
        return gson.toJson(obj);
    }
    public String listToJson(List<?> list){
        return gson.toJson(list);
    }
    public Pair<String, AssistantParent> jsonObjectToObject(JsonObject jsonObject){
        logger.debug("Parsing json object {}...", jsonObject);
        String feature = jsonObject.get("key").getAsString();
        if(feature == null )
        {
            logger.error("No 'key' element found in json content.");
            return null;
        }
        Pair<String, AssistantParent> pair = new Pair<String, AssistantParent>(null, null);
        if(!services.containsService(feature)){
            logger.error("Feature given not in support.");
            return null;
        }
        JsonObject payload = jsonObject.getAsJsonObject("value");
        if(payload == null ){
            logger.error("No 'value' element found in json content.");
            return null;
        }
        AssistantParent value = (AssistantParent) gson.fromJson(payload, services.getAssistant(feature));
        pair.setKey(feature);
        pair.setValue(value);
        logger.debug("Parsing json object done.");
        return pair;
    }
    public Object featureListJsonToObject(String json){
        logger.debug("Converting json string {} to Object... ", json);
        List<JsonObject> objList = gson.fromJson(json, new TypeToken<List<JsonObject>>(){}.getType());
        List<Pair<String, AssistantParent>> list = new ArrayList<>();
        for(int i =0 ; i< objList.size(); i++){
            Pair<String, AssistantParent> pair = jsonObjectToObject(objList.get(i));
            list.add(pair);
        }
        logger.debug("Converting json string done... ");
        return list;
    }
}
