package org.opendaylight.controller.pnp.northbound;

import java.util.ArrayList;
import java.util.List;

import org.opendaylight.controller.northbound.commons.RestMessages;
import org.opendaylight.controller.northbound.commons.exception.ResourceNotFoundException;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.objects.api.ServiceEnum;
import org.opendaylight.controller.pnp.storage.api.DeviceKey;
import org.opendaylight.controller.pnp.storage.api.DeviceStatus;
import org.opendaylight.controller.pnp.storage.api.IMasterDB;
import org.opendaylight.controller.pnp.storage.api.IPnPReflection;
import org.opendaylight.controller.pnp.storage.api.Pair;
import org.opendaylight.controller.pnp.storage.api.PnPDBOperations;
import org.opendaylight.controller.pnp.storage.api.ServiceKey;
import org.opendaylight.controller.sal.utils.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PnPNorthboundImpl {
    protected static final Logger logger = LoggerFactory
        .getLogger(PnPNorthboundImpl.class);
    protected static final String DBPREFIX = "pnp.storage.";
    protected static final String DBPOSTFIX = "DB";

    protected static final Integer DAYONE = 0;
    protected static final Integer DAYTWOWAITING = 1;
    protected static final Integer DAYTWODONE = 2;

    protected static final String FAULT="fault";
    protected JSONConverter converter;
    protected PnPDBOperations dbs;
    protected PnpServiceList services;

    protected IPnPReflection ref;

    public PnPNorthboundImpl(){
        converter = new JSONConverter();
        this.dbs = (PnPDBOperations) ServiceHelper.getGlobalInstance(PnPDBOperations.class,
                this);
        if (this.dbs == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
        this.services = (PnpServiceList) ServiceHelper.getGlobalInstance(PnpServiceList.class,
                this);
        if (this.services == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }

        this.ref = (IPnPReflection) ServiceHelper.getGlobalInstance(IPnPReflection.class,
                this);
        if (this.ref == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
    }
    public List<Object> loadTable(String tableId){
        logger.info("load table {}.", tableId);
        List<Object> uiEntries = new ArrayList<>();
        if(tableId.equals("master")){
            List<IMasterDB> entries = (List<IMasterDB>) dbs.getTable(getMasterCacheName());
            for (int iter = 0; iter < entries.size(); iter ++){
                MasterEntry entry = new MasterEntry();
                entry.fromDBEntry(entries.get(iter));
                uiEntries.add(entry);
            }
        }
        return uiEntries;
    }
    public MasterEntry loadDeviceEntry(String udi){
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return null;
            }
            Object res = dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device UDI not exists in DB: {}", udi);
                return null;
            }
            MasterEntry masterEntry = new MasterEntry();
            masterEntry.fromDBEntry((IMasterDB)res);
            return masterEntry;
        }
        logger.error("Device udi provided is null");
        return null;
    }
    public String getCacheName(String id){
        return DBPREFIX+id;
    }
    public String getMasterCacheName(){
        return DBPREFIX+"master"+DBPOSTFIX;
    }
    public Boolean addDevice(MasterEntry entry) {
        String dbName = getMasterCacheName();
        logger.info("Attempt to add device {} to {}.", entry, dbName);
        IMasterDB dbEntry = (IMasterDB) entry.toDBEntry();
        dbEntry.setClaimedFlag(true);
        if(dbs.entryCreate(dbEntry.getDkey(),dbEntry , dbName)){
            logger.info("Entry created success.");
            return true;
        }
        else{
            logger.error("Fail to create entry for device {} to {}.", entry, dbName );
            return false;
        }
    }
    public Boolean deleteDevice(String udi ){
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("Device UDI provided is in malicious format: {}", udi);
                return false;
            }
            Object res = dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device UDI not exists in DB: {}", udi);
                return false;
            }
            IMasterDB dbEntry = (IMasterDB)res;
            logger.info("Attempt to delete features for device {} .", udi);
            if(!deleteAllFeatures(dbEntry)){
                logger.error("Fail to delete features for entry {}.", dbEntry);
                return false;
            }

            if(!dbs.entryDelete(dkey ,  getMasterCacheName())){
                logger.error("Fail to delete device entry {}.", dkey);
                return false;
            }
            logger.info("Device {} is deleted completely .", udi);
            return true;

        }
        logger.error("Device udi provided is null");
        return false;
    }
    public Boolean deleteFeatures(IMasterDB dbEntry, Integer dynamicFlag){
        ArrayList<String> features = null;
        if(dynamicFlag == DAYONE)
        {
            if(dbEntry.getSequence() == null){
                logger.info("Day0:No features configured yet.");
                return true;
            }else
                features = dbEntry.getSequence();

        }else if(dynamicFlag ==DAYTWOWAITING){
            if(dbEntry.getDynamicSequence() == null){
                logger.info("Day2:No feature waiting.");
                return true;
            }else
                features = dbEntry.getDynamicSequence();
        }else if(dynamicFlag ==DAYTWODONE){
            if(dbEntry.getHistorySequence() == null){
                logger.info("Day2:No feature deployed.");
                return true;
            }else
                features = dbEntry.getHistorySequence();
        }

        for(int i=0; i< features.size(); i++){
            ServiceKey skey = new ServiceKey(dbEntry.getDkey(), i, features.get(i), dynamicFlag);
            if(!dbs.entryDelete(skey, getCacheName(features.get(i)))){
                logger.error("Delete features in {} failed", getCacheName(features.get(i)));
                return false;
            }

        }
        if(dynamicFlag == DAYONE){
           dbEntry.setSequence(null);
        }else if(dynamicFlag ==DAYTWOWAITING){
            dbEntry.setDynamicSequence(null);
        }else if(dynamicFlag ==DAYTWODONE){
            dbEntry.setHistorySequence(null);
        }
        return true;
    }

    public Boolean deleteAllFeatures(IMasterDB dbEntry){
        if(!deleteFeatures(dbEntry,DAYONE)){
            logger.error("Delete all day0 features fails");
            return false;
        }
        if(!deleteFeatures(dbEntry,DAYTWOWAITING)){
            logger.error("Delete all day2 waiting features fails");
            return false;
        }

        if(!deleteFeatures(dbEntry,DAYTWODONE)){
            logger.error("Delete all day2 historical features fails");
            return false;
        }
        return true;
    }
    public Boolean updateDevice(MasterEntry entry){
        String udi = entry.getUdi();
        logger.info("Device {} attempted to be updated.", udi);
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return false;
            }
            Object res =  dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device UDI not exists in DB: {}", udi);
                return false;
            }else{
                entry.updateDBEntry((IMasterDB)res);
                logger.info("Device {} exists, updated.", res);
                return true;
            }
        }
        logger.error("Device udi provided is null");
        return false;
    }
    public Boolean resetStatus(String udi){
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return false;
            }
            Object res =  dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device UDI not exists in DB: {}", udi);
                return false;
            }else{
                IMasterDB entry = (IMasterDB)res;
                if(!resetError(entry)){
                    logger.error("Reset status failed for device {}", udi);
                    return false;
                }
                logger.info("Device {} exists, reset status.", res);
                return true;
            }
        }
        logger.error("Device udi provided is null");
        return false;
    }
    public Boolean resetError(IMasterDB entry){
        entry.setErrorCode("");
        entry.setErrorMessage("");
        entry.setErrorSeverity("");
        if(entry.getStatus().equals(DeviceStatus.ODL_PNP_ERROR)
                || entry.getStatus().equals(DeviceStatus.ODL_PNP_FAULT)){
            if(entry.getSequence() == null || entry.getSequence().size() == 0){
                entry.setStatus(DeviceStatus.ODL_PNP_INITIAL);
            }else if(entry.getNext() == 0){
                entry.setStatus(DeviceStatus.ODL_PNP_INITIAL);
            }else if( entry.getNext() < entry.getSequence().size()){
                entry.setStatus(DeviceStatus.ODL_PNP_DEPLOYING);
            }
        }
        if(entry.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_ERROR)
                || entry.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_FAULT))
        {
            if(entry.getDynamicSequence().size() == 0)
                entry.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_NO_JOB);
            else
                entry.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_JOB_WAITING);
        }
        entry.setFaultFlag(false);
        ServiceKey skey = new ServiceKey(entry.getDkey(),0, FAULT, DAYONE);
        if(dbs.entryLookUp(skey, getCacheName(FAULT)) != null && !dbs.entryDelete(skey, getCacheName(FAULT))){
            logger.error("Day0: Delete fault record failed!");
            return false;
        }
        skey = new ServiceKey(entry.getDkey(),0, FAULT, DAYTWOWAITING);
        if(dbs.entryLookUp(skey, getCacheName(FAULT)) != null && !dbs.entryDelete(skey, getCacheName(FAULT))){
            logger.error("Day2: Delete fault record failed!");
            return false;
        }
        return true;
    }
    public Boolean claimDevice(String udi){
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return false;
            }
            Object res =  dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device UDI not exists in DB: {}", udi);
                return false;
            }else{
                IMasterDB entry = (IMasterDB)res;
                entry.setClaimedFlag(true);
                logger.info("Device {} exists, claim it.", res);
                return true;
            }
        }
        logger.error("Device udi provided is null");
        return false;
    }
    public Boolean reloadDevice(String udi) throws InstantiationException, IllegalAccessException{
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return false;
            }
            Object res =  dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device UDI not exists in DB: {}", udi);
                return false;
            }else{
                IMasterDB entry = (IMasterDB)res;
                if(!resetError(entry)){
                    logger.error("Error status reset failed");
                    return false;
                }
                entry.setStatus(DeviceStatus.ODL_PNP_WAITING_FOR_RELOAD);
                entry.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_NO_JOB);
                if(!deleteAllFeatures(entry)){
                    logger.error("Fail to delete all features for entry {}.", entry);
                    return false;
                }
                entry.setNext(0);
                if(entry.getSequence()!= null)
                   entry.getSequence().add("reload");
                else{
                    ArrayList<String> list = new ArrayList<>();
                    list.add("reload");
                    entry.setSequence(list);
                }
                AssistantParent ap= (AssistantParent) services.getAssistant(ServiceEnum.ODL_PNP_RELOAD.getService()).newInstance();
                ServiceKey skey = new ServiceKey(dkey, 0, ServiceEnum.ODL_PNP_RELOAD.getService(), DAYONE);
                if(!dbs.entryCreate(skey, ap, getCacheName(ServiceEnum.ODL_PNP_RELOAD.getService()))){
                    logger.error("Create reload entry failed!");
                }
                logger.info("Successfully set reload request.Remove all other features.");
                return true;
            }
        }
        logger.error("Device udi provided is null");
        return false;
    }
    public List<Pair<String,AssistantParent>> loadFeatures(String udi, Integer dynamicFlag){
        List<Pair<String,AssistantParent>> list = new ArrayList<>();
        logger.info("Attempt to get features for device {}." , udi);
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("Day0:udi {} not in proper format! ", udi);
                return null;
            }
            Object res = dbs.entryLookUp(dkey, getMasterCacheName());
            if(res!= null ){
                IMasterDB dbEntry = (IMasterDB) res;
                ArrayList<String> features = null;
                if(dynamicFlag == DAYONE)
                {
                    if(dbEntry.getSequence() == null){
                        logger.info("Day0:No features configured yet.");
                        return list;
                    }else
                        features = dbEntry.getSequence();

                }else if(dynamicFlag == DAYTWOWAITING){
                    if(dbEntry.getDynamicSequence() == null){
                        logger.info("Day2:No feature waiting.");
                        return list;
                    }else
                        features = dbEntry.getDynamicSequence();
                }else if(dynamicFlag ==DAYTWODONE){
                    if(dbEntry.getHistorySequence() == null){
                        logger.info("Day2:No feature deployed.");
                        return list;
                    }else
                        features = dbEntry.getHistorySequence();
                }


                for(int i=0; i< features.size(); i++){
                    ServiceKey skey = new ServiceKey(dkey, i, features.get(i),dynamicFlag);
                    Object res_f = dbs.entryLookUp(skey, getCacheName(features.get(i)));
                    if(res_f != null){
                        logger.info("Loading features in db {}", getCacheName(features.get(i)));
                        list.add(new Pair<String,AssistantParent>(features.get(i),
                                    (AssistantParent)res_f));
                    }
                }
                return list;
            }
            logger.error("Day0: Device {} not exists." , udi);
            return null;
        }
        return null;
    }

    public AssistantParent loadDeviceInfo(String udi){
        logger.info("Attempt to get device info for device {}." , udi);
        AssistantParent result = null;
        String ser =  "device-info";
        if(udi !=null)
        {
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return null;
            }
            Object res = dbs.entryLookUp(dkey, getMasterCacheName());
            if(res!= null ){
                IMasterDB dbEntry = (IMasterDB) res;
                ServiceKey skey = new ServiceKey(dkey, 0, ser,0);
                Object res_f = dbs.entryLookUp(skey, getCacheName(ser));
                if(res_f != null){
                    logger.info("Loading features in db {}", getCacheName(ser));
                    result = (AssistantParent)res_f;
                }

                return result;
            }
        }
        logger.error("Device {} not exists." , udi);
        return null;
    }
    public Boolean updateFeature( String udi, List<Pair<String,AssistantParent> > list, Integer dynamicFlag){
        if( udi == null ){
            logger.error("Device udi is null");
            return false;
        }
        DeviceKey dkey = new DeviceKey();
        if(!dkey.parseUdi(udi)){
            logger.error("udi {} not in proper format! ", udi);
            return false;
        }
        ArrayList<String> newSeq = new ArrayList<>();
        // delete previous one
        Object res = dbs.entryLookUp(dkey, getMasterCacheName());
        if(res == null){
            logger.error("Device {} do not exists", udi);
            return false;
        }
        IMasterDB dbEntry = (IMasterDB) res;

        ArrayList<String> originSeq = null;
        if(dynamicFlag == DAYONE){
            if(dbEntry.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)){
                logger.info("Day one already deployed! Update feature not allowed unless reload to initial stage");
                return false;
            }
            originSeq = dbEntry.getSequence();
        }else
            originSeq = dbEntry.getDynamicSequence();

        for(int i = 0; i< list.size(); i++){
            Pair<String, AssistantParent> pair  = list.get(i);
            newSeq.add(pair.getKey());
            if(originSeq!=null && i< originSeq.size() && ! originSeq.get(i).equals(pair.getKey())){
                ServiceKey skey_ori = new ServiceKey(dkey, i, originSeq.get(i), dynamicFlag);
                dbs.entryDelete(skey_ori, getCacheName(originSeq.get(i)));
                logger.info("Device {}: Feature {} is deleted", dkey, skey_ori );
            }
            ServiceKey skey = new ServiceKey(dkey, i, pair.getKey(), dynamicFlag);
            Object res_f = dbs.entryLookUp(skey, getCacheName(pair.getKey()));
            if(res_f != null){
                if(((AssistantParent)res_f).hashCode()!= pair.getValue().hashCode())
                {
                    dbs.entryUpdate(skey, pair.getValue(), getCacheName(pair.getKey()));
                    logger.info("Device {}: Feature {} updated ", dkey, skey);
                }
            }else
                dbs.entryCreate(skey, pair.getValue(), getCacheName(pair.getKey()));
        }
        if(dynamicFlag == DAYONE ){
            dbEntry.setNext(0);
            dbEntry.setSequence(newSeq);
        }else{
            if(newSeq.size()>0){
                dbEntry.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_JOB_WAITING);
            }
            dbEntry.setDynamicSequence(newSeq);
        }
        logger.info("Device {} : features updated successfully", dkey);
        return true;
    }
    public AssistantParent loadFault(String udi, Integer dynamicFlag) {
         if( udi == null ){
                logger.error("Device udi is null");
                return null;
            }
            DeviceKey dkey = new DeviceKey();
            if(!dkey.parseUdi(udi)){
                logger.error("udi {} not in proper format! ", udi);
                return null;
            }
            Object res = dbs.entryLookUp(dkey, getMasterCacheName());
            if(res == null){
                logger.error("Device {} do not exists", udi);
                return null;
            }
            IMasterDB dbEntry = (IMasterDB) res;
            if(dbEntry.getFaultFlag() == false){
                logger.error("There is no fault for device {}", udi);
                return null;
            }
        if(dynamicFlag == DAYONE){
            if(dbEntry.getStatus().equals(DeviceStatus.ODL_PNP_FAULT)){
               ServiceKey skey = new ServiceKey(dkey, 0, ServiceEnum.ODL_PNP_FAULT.getService(),DAYONE);
               return (AssistantParent) dbs.entryLookUp(skey, DBPREFIX+ServiceEnum.ODL_PNP_FAULT.getService());
            }else{
                logger.error("Day1 {}: Normal status, no fault", udi);
                return null;
                }
        }else{
              if(dbEntry.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_FAULT)){
                  ServiceKey skey = new ServiceKey(dkey, 0, ServiceEnum.ODL_PNP_FAULT.getService(),DAYTWOWAITING);
                  return (AssistantParent) dbs.entryLookUp(skey, DBPREFIX+ServiceEnum.ODL_PNP_FAULT.getService());
               }else{
                logger.error("Day2 {}: Normal status, no fault", udi);
                   return null;
               }
        }
    }
}
