package org.opendaylight.controller.pnp.storage.api;

import junit.framework.Assert;

import org.junit.Test;

public class DeviceKeyTest {
     @Test
     public void testConstructorFromWrongUdi(){
         String str = "adhahdasd";
         DeviceKey dkey = new DeviceKey();
         Assert.assertFalse(dkey.parseUdi(str));
     }
     @Test
     public void testConstructorFromCorrectUdi(){
         String str = "PID:WS-C3560C-12PC-S,VID:01,SN:FOC1827Y5EJ";
         DeviceKey dkey = new DeviceKey();
         System.out.println(dkey.parseUdi(str));
     //    Assert.assertTrue(dkey.parseUdi(str));
     }
}
