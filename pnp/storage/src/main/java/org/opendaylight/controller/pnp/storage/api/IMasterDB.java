package org.opendaylight.controller.pnp.storage.api;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.opendaylight.controller.configuration.ConfigurationObject;
import org.opendaylight.controller.pnp.storage.impl.PnPReflection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class IMasterDB extends ConfigurationObject implements Serializable {
    protected static final long serialVersionUID = 1L;
    protected static final Logger log = LoggerFactory.getLogger(IMasterDB.class);
    protected DeviceKey dkey = null;
    protected String deviceName = null;
    protected String location = null;
    protected String errorCode = null;
    protected String errorMessage = null;
    protected String errorSeverity = null;
    protected Boolean faultFlag = false;
    protected Boolean authRequired = false;
    protected Boolean claimedFlag = false;
    protected String usr = null;
    protected String pwd = null;
    protected String version = null;
    protected Boolean viaProxy = false;
    protected String securityAdvise = null;
    protected String correlator = null;
    protected DeviceStatus status = DeviceStatus.ODL_PNP_INITIAL;
    //  protected Boolean flag = false;
    protected ArrayList<String> sequence = null;
    protected ArrayList<String> dynamicSequence = null;
    protected ArrayList<String> historySequence = null;
    protected DeviceStatus dynamicStatus= DeviceStatus.ODL_PNP_DAYTWO_NO_JOB;
    //  protected String capability = null;
    protected Integer next = 0;
    //  protected Boolean ignoreFlag = false;
    //  protected String dynamicFlag = null ;
    public IMasterDB(){}

    public Boolean parseEntry(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        PnPReflection ref = new PnPReflection();
        Object info= ref.hasReturnReflection(obj,"getInfo");
        if(info == null){
            log.error("No {} content in {}", "info", obj);
            return false;
        }
        Object corObj = ref.hasReturnReflection(info, "getCorrelator");
        if(corObj == null){
            log.error("No {} content in {}", "correlator", info);
            return false;
        }
        this.correlator = (String) corObj;
        Object deviceId = ref.hasReturnReflection(info ,"getDeviceId");
        if(deviceId == null){
            log.error("No {} content in {}", "deviceId", info);
            return false;
        }
        Object udi = ref.hasReturnReflection(deviceId, "getUdi");
        Object authRequired = ref.hasReturnReflection(deviceId, "isAuthRequired");
        Object securityAdvise =ref.hasReturnReflection(deviceId, "getSecurityAdvise");
        Object viaProxy = ref.hasReturnReflection(deviceId, "isViaProxy");
        if(udi == null){
            log.error("No {} content in {}", "deviceId", info);
            return false;
        }
        if(this.dkey == null){
            this.dkey = new DeviceKey();
            if(!dkey.parseUdi((String)udi)){
                log.error("UDI {} is in wrong format!", udi);
                return false;
            }
        }
        Object tmp = ref.hasReturnReflection(obj, "getVersion");
        if(tmp != null){
            this.version = (String) tmp;
        }
        tmp = ref.hasReturnReflection(obj,"getUsr");
        if(tmp != null){
            this.usr = (String) tmp;
        }
        tmp = ref.hasReturnReflection(obj,"getPwd");
        if(tmp != null){
            this.pwd = (String) tmp;
        }
        if(authRequired != null)
            this.authRequired = (Boolean) authRequired;
        if(securityAdvise != null)
            this.securityAdvise = (String) securityAdvise;
        if(viaProxy != null)
            this.viaProxy = (Boolean) viaProxy;
        return true;
    }
    public void setDkey(DeviceKey dkey){
        this.dkey = dkey;
    }
    public void setDeviceName(String deviceName){
        this.deviceName = deviceName;
    }
    public void setLocation(String location){
        this.location = location;
    }

    public DeviceKey getDkey(){
        return this.dkey ;
    }

    public String getDeviceName(){
        return this.deviceName ;
    }
    public String getLocation(){
        return this.location;
    }

    public ArrayList<String> getSequence(){
        return this.sequence;
    }
    public void setSequence(ArrayList<String> sequence){
        this.sequence = sequence;
    }
    public void setNext(Integer next){
        this.next = next;
    }
    public Integer getNext(){
        return this.next;
    }
    public void setStatus(DeviceStatus status ){
        this.status = status;
    }
    public DeviceStatus getStatus(){
        return this.status;
    }
    public void setErrorCode(String errorCode){
        this.errorCode = errorCode;
    }
    public String getErrorCode(){
        return this.errorCode;
    }
    public void setErrorMessage(String errorMsg){
        this.errorMessage = errorMsg;
    }
    public String getErrorMessage(){
        return this.errorMessage;
    }
    public void setErrorSeverity(String errorSeverity){
        this.errorSeverity = errorSeverity;
    }
    public String getErrorSeverity(){
        return this.errorSeverity;
    }

    public void setVersion(String version){
        this.version = version;
    }
    public String getVersion(){
        return this.version;
    }
    public String getUsr() {
        return usr;
    }

    public void setUsr(String value) {
        this.usr = value;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String value) {
        this.pwd = value;
    }
    public void setCorrelator(String value){
        this.correlator = value;
    }
    public String getCorrelator(){
        return this.correlator;
    }
    public Boolean isAuthRequired() {
        return authRequired;
    }
    public Boolean isViaProxy() {
        return viaProxy;
    }

    public void setViaProxy(Boolean value) {
        this.viaProxy = value;
    }


    public String getSecurityAdvise() {
        return securityAdvise;
    }


    public void setSecurityAdvise(String value) {
        this.securityAdvise = value;
    }

    public void setAuthRequired(Boolean value) {
        this.authRequired = value;
    }
    public void setClaimedFlag(Boolean value){

        this.claimedFlag = value;

    }

    public Boolean getClaimedFlag(){

        return this.claimedFlag;
    }
    public void setFaultFlag(Boolean value){

        this.faultFlag = value;

    }

    public Boolean getFaultFlag(){

        return this.faultFlag;
    }
    public void setDynamicSequence(ArrayList<String> value){

        this.dynamicSequence = value;

    }

    public ArrayList<String> getDynamicSequence(){

        return this.dynamicSequence;
    }
    public void setHistorySequence(ArrayList<String> value){

        this.historySequence = value;

    }

    public ArrayList<String> getHistorySequence(){

        return this.historySequence;
    }
    public void setDynamicStatus(DeviceStatus value){

        this.dynamicStatus = value;

    }

    public DeviceStatus getDynamicStatus(){

        return this.dynamicStatus;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result= prime*result+((dkey == null) ? 0 : dkey.hashCode());

        result= prime*result+((deviceName == null) ? 0 : deviceName.hashCode());

        result= prime*result+((location == null) ? 0 : location.hashCode());

        result= prime*result+((errorCode == null) ? 0 : errorCode.hashCode());

        result= prime*result+((errorMessage == null) ? 0 : errorMessage.hashCode());

        result= prime*result+((errorSeverity == null) ? 0 : errorSeverity.hashCode());

        result= prime*result+((authRequired == null) ? 0 : authRequired.hashCode());

        result= prime*result+((usr == null) ? 0 : usr.hashCode());

        result= prime*result+((pwd == null) ? 0 : pwd.hashCode());

        result= prime*result+((version == null) ? 0 : version.hashCode());

        result= prime*result+((viaProxy == null) ? 0 : viaProxy.hashCode());

        result= prime*result+((securityAdvise == null) ? 0 : securityAdvise.hashCode());

        result= prime*result+((status == null) ? 0 : status.hashCode());

        result= prime*result+((sequence == null) ? 0 : sequence.hashCode());

        result= prime*result+((correlator == null) ? 0 : correlator.hashCode());

        result= prime*result+((next == null) ? 0 : next.hashCode());

        result= prime*result+((claimedFlag == null) ? 0 : claimedFlag.hashCode());

        result= prime*result+((faultFlag == null) ? 0 : faultFlag.hashCode());

        result= prime*result+((dynamicSequence == null) ? 0 : dynamicSequence.hashCode());

        result= prime*result+((historySequence == null) ? 0 : historySequence.hashCode());

        result= prime*result+((dynamicStatus == null) ? 0 : dynamicStatus.hashCode());


        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } IMasterDB other = (IMasterDB) obj;
        if (dkey == null) {
            if (other.getDkey() != null) {
                return false;
            }
        } else if (!dkey.equals(other.getDkey())) {
            return false;
        }

        if (deviceName == null) {
            if (other.getDeviceName() != null) {
                return false;
            }
        } else if (!deviceName.equals(other.getDeviceName())) {
            return false;
        }

        if (location == null) {
            if (other.getLocation() != null) {
                return false;
            }
        } else if (!location.equals(other.getLocation())) {
            return false;
        }

        if (errorCode == null) {
            if (other.getErrorCode() != null) {
                return false;
            }
        } else if (!errorCode.equals(other.getErrorCode())) {
            return false;
        }

        if (errorMessage == null) {
            if (other.getErrorMessage() != null) {
                return false;
            }
        } else if (!errorMessage.equals(other.getErrorMessage())) {
            return false;
        }

        if (errorSeverity == null) {
            if (other.getErrorSeverity() != null) {
                return false;
            }
        } else if (!errorSeverity.equals(other.getErrorSeverity())) {
            return false;
        }

        if (authRequired == null) {
            if (other.isAuthRequired() != null) {
                return false;
            }
        } else if (!authRequired.equals(other.isAuthRequired())) {
            return false;
        }

        if (usr == null) {
            if (other.getUsr() != null) {
                return false;
            }
        } else if (!usr.equals(other.getUsr())) {
            return false;
        }

        if (pwd == null) {
            if (other.getPwd() != null) {
                return false;
            }
        } else if (!pwd.equals(other.getPwd())) {
            return false;
        }
        if (correlator == null) {
            if (other.getCorrelator() != null) {
                return false;
            }
        } else if (!correlator.equals(other.getCorrelator())) {
            return false;
        }

        if (version == null) {
            if (other.getVersion() != null) {
                return false;
            }
        } else if (!version.equals(other.getVersion())) {
            return false;
        }

        if (viaProxy == null) {
            if (other.isViaProxy() != null) {
                return false;
            }
        } else if (!viaProxy.equals(other.isViaProxy())) {
            return false;
        }

        if (securityAdvise == null) {
            if (other.getSecurityAdvise() != null) {
                return false;
            }
        } else if (!securityAdvise.equals(other.getSecurityAdvise())) {
            return false;
        }

        if (status == null) {
            if (other.getStatus() != null) {
                return false;
            }
        } else if (!status.equals(other.getStatus())) {
            return false;
        }

        if (sequence == null) {
            if (other.getSequence() != null) {
                return false;
            }
        } else if (!sequence.equals(other.getSequence())) {
            return false;
        }

        if (next == null) {
            if (other.getNext() != null) {
                return false;
            }
        } else if (!next.equals(other.getNext())) {
            return false;
        }
        if (claimedFlag == null) {
            if (other.getClaimedFlag() != null) {
                return false;
            }
        } else if (!claimedFlag.equals(other.getClaimedFlag())) {
            return false;
        }
        if (faultFlag == null) {
            if (other.getFaultFlag() != null) {
                return false;
            }
        } else if (!faultFlag.equals(other.getFaultFlag())) {
            return false;
        }

        if (dynamicSequence == null) {
            if (other.getDynamicSequence() != null) {
                return false;
            }
        } else if (!dynamicSequence.equals(other.getDynamicSequence())) {
            return false;
        }

        if (historySequence == null) {
            if (other.getHistorySequence() != null) {
                return false;
            }
        } else if (!historySequence.equals(other.getHistorySequence())) {
            return false;
        }
        if (dynamicStatus == null) {
            if (other.getDynamicStatus() != null) {
                return false;
            }
        } else if (!dynamicStatus.equals(other.getDynamicStatus())) {
            return false;
        }


        return true;
    }

    @Override
    public String toString(){
        return "[ dkey= :{" + dkey + "} deviceName= :{" + deviceName + "} location= :{" + location
            + "} errorCode= :{" + errorCode + "} errorMessage= :{" + errorMessage + "} errorSeverity= :{" + errorSeverity
            + "} authRequired= :{" + authRequired + "} usr= :{" + usr + "} pwd= :{" + pwd +"} correlator= :{"+ correlator
            + "} version= :{" + version + "} viaProxy= :{" + viaProxy + "} securityAdvise= :{" + securityAdvise + "} status= :{"
            + status + "} sequence= :{" + sequence + "} next= :{" + next + "} claimedFlag= :{" + claimedFlag
            + "} faultFlag= :{" + faultFlag + "} dynamicSequence= :{" + dynamicSequence + "} historySequence= :{" + historySequence
            + "} dynamicStatus= :{" + dynamicStatus +"} }]"; }

}
