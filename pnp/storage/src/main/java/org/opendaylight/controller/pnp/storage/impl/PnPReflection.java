package org.opendaylight.controller.pnp.storage.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.opendaylight.controller.pnp.storage.api.IPnPReflection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PnPReflection implements IPnPReflection{
    protected static final Logger log = LoggerFactory.getLogger(PnPReflection.class);
    @Override
    public Object hasReturnReflection(Object obj, String name) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException  {
        Object result = null;
        Method method = null;
        method = obj.getClass().getMethod(name);
        if(method == null){
            log.error("No {} method in Class {}, reflection call of {} failed.", name, obj.getClass(), obj );
            return null;
        }
        result = method.invoke(obj);
        return result;
    }
    @Override
    public Boolean noReturnReflection(Object obj, String name, Class<?> cls, Object arg) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method = obj.getClass().getMethod(name, cls);
        if(method == null){
            log.error("No {} method in Class {}, reflection call of {} failed.", name, obj.getClass(), obj );
            return false;
        }
        method.invoke(obj, arg);
        return true;
    }

}
