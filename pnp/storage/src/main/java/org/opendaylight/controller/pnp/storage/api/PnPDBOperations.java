package org.opendaylight.controller.pnp.storage.api;

import java.util.List;


public interface PnPDBOperations {

    public Boolean entryCreate(Object key, Object value, String cacheName);

    public Boolean entryUpdate(Object key, Object new_value, String cacheName);

    public Boolean entryDelete(Object key, String cacheName);

    public Boolean entryDeleteByValue(Object value, String cacheName);

    public Object entryLookUp(Object key, String cacheName);

    public List<?> getTable(String cacheName);
}
