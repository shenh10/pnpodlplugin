package org.opendaylight.controller.pnp.storage.api;

import java.io.Serializable;

import org.opendaylight.controller.configuration.ConfigurationObject;

public class ServiceKey extends ConfigurationObject implements Serializable{
    private final static long serialVersionUID = 1L;
    private DeviceKey dkey;
    private Integer step;
    private String service;
    private Integer dynamicFlag = 0;
    public ServiceKey(DeviceKey dkey, Integer step, String service, Integer dynamicFlag){
        this.dkey = dkey;
        this.step = step;
        this.service = service;
        this.dynamicFlag = dynamicFlag;
    }
    public void setDKey(DeviceKey dkey){
        this.dkey = dkey;
    }
    public DeviceKey getDkey(){
        return this.dkey;
    }
    public void setStep(Integer step){
        this.step = step;
    }
    public Integer getStep(){
        return this.step;
    }
    public void setService(String service){
        this.service = service;
    }
    public String getService(){
        return this.service;
    }
    public void setDynamicFlag(Integer value){

        this.dynamicFlag = value;

    }

    public Integer getDynamicFlag(){

        return this.dynamicFlag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((dkey == null) ? 0 : dkey.hashCode());
        result = prime * result + ((step == null) ? 0 : step.hashCode());
        result = prime * result + ((service == null) ? 0 : service.hashCode());
        result= prime*result+((dynamicFlag == null) ? 0 : dynamicFlag.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ServiceKey  other = (ServiceKey) obj;
        if (dkey == null) {
            if (other.getDkey() != null) {
                return false;
            }
        } else if (! dkey.equals(other.getDkey())) {
            return false;
        }
        if (service == null) {
            if (other.getService() != null) {
                return false;
            }
        } else if (!service.equals(other.getService())) {
            return false;
        }
        if (step == null) {
            if (other.getStep() != null) {
                return false;
            }
        } else if (!step.equals(other.getStep())) {
            return false;
        }
        if (dynamicFlag == null) {
            if (other.getDynamicFlag() != null) {
                return false;
            }
        } else if (!dynamicFlag.equals(other.getDynamicFlag())) {
            return false;
        }

        return true;
    }
    @Override
    public String toString(){
        return " [DeviceKey=" + dkey + ", Step ="+ step +",Service =" +service +"} dynamicFlag= :{" + dynamicFlag + "} ]";
    }

}
