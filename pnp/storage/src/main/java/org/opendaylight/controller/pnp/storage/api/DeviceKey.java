package org.opendaylight.controller.pnp.storage.api;

import java.io.Serializable;

import org.opendaylight.controller.configuration.ConfigurationObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceKey extends ConfigurationObject implements Serializable{
    private final static long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(DeviceKey.class);

    private String pid = null;
    private String vid = null;
    private String sn = null;
    public DeviceKey(){}
    public DeviceKey(String pid, String vid , String sn, int index){
        this.pid = pid;
        this.vid = vid;
        this.sn = sn;
    }
    public Boolean parseUdi(String udi){
        String[] spl1 = udi.split(",");
        if(spl1.length!=3){
            log.error("{}. Udi in wrong format :{}. it should follow format PID:$(pid),VID:$(vid),SN:$(sn)",
                    "Comma splitting error", udi);
            return false;
        }
        String[] tmp;
        tmp = spl1[0].split(":");
        this.pid  = subStringFormatCheck(tmp, udi, "PID");
        if(this.pid == null)
            return false;
        tmp = spl1[1].split(":");
        this.vid  = subStringFormatCheck(tmp, udi, "VID");

        if(this.vid == null){
            return false;
        }
        tmp = spl1[2].split(":");
        this.sn = subStringFormatCheck(tmp, udi, "SN");
        if(this.sn == null){
            return false;
        }
        return true;
    }
    public String subStringFormatCheck(String[] split, String udi,String propName){
        String property = null;
        if(split.length>2){
            log.error("{}.Udi in wrong format : {}. it should follow format PID:$(pid),VID:$(vid),SN:$(sn)",
                    "Colon splitting error",udi);
            return null;
        }else if(split.length == 2 && split[0].equals(propName))
            property = split[1];
        else if(split.length == 1 && split[0].equals(propName))
            property = "";
        else {
            log.error("{}.Udi in wrong format : {}. it should follow format PID:$(pid),VID:$(vid),SN:$(sn)",
                    "Colon splitting error", udi);
            return null;
        }
        return property;
    }
    public String getPid(){
        return this.pid;
    }
    public void setPid(String pid){
        this.pid = pid;
    }
    public String getVid(){
        return this.vid;
    }
    public void setVid(String vid){
        this.vid = vid;
    }
    public String getSN(){
        return this.sn;
    }
    public void setSN(String sn){
        this.sn = sn;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((pid == null) ? 0 : pid.hashCode());
        result = prime * result + ((vid == null) ? 0 : vid.hashCode());
        result = prime * result + ((sn == null) ? 0 : sn.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DeviceKey  other = (DeviceKey) obj;
        if (pid == null) {
            if (other.getPid() != null) {
                return false;
            }
        } else if (!pid.equals(other.getPid())) {
            return false;
        }
        if (vid == null) {
            if (other.getVid() != null) {
                return false;
            }
        } else if (!vid.equals(other.getVid())) {
            return false;
        }
        if (sn == null) {
            if (other.getSN() != null) {
                return false;
            }
        } else if (!sn.equals(other.getSN())) {
            return false;
        }
        return true;
    }
    @Override
    public String toString(){
        return " [PID=" + pid + ",VID="+ vid +",SN=" +sn +"]";
    }
    public String toUdi(){
        return "PID:" + pid + ",VID:"+ vid +",SN:" +sn ;
    }
}
