package org.opendaylight.controller.pnp.storage.impl;

import org.apache.felix.dm.Component;
import org.opendaylight.controller.clustering.services.IClusterGlobalServices;
import org.opendaylight.controller.configuration.IConfigurationService;
import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.objects.api.ICustomedConstructor;
import org.opendaylight.controller.pnp.storage.api.IPnPReflection;
import org.opendaylight.controller.pnp.storage.api.PnPDBOperations;
import org.opendaylight.controller.sal.core.ComponentActivatorAbstractBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Activator extends ComponentActivatorAbstractBase{
    protected static final Logger logger = LoggerFactory
               .getLogger(Activator.class);
      @Override
       public Object[] getImplementations() {
         return new Object[]{};
      }
      @Override
       public void configureInstance(Component c, Object imp, String containerName) {
       }
      @Override
       protected Object[] getGlobalImplementations() {
           Object[] res = { PnPDB.class, PnPReflection.class};
           return res;
       }
      @Override
       protected void configureGlobalInstance(Component c, Object imp) {
           if (imp.equals(PnPDB.class)) {

               // export the service
               c.setInterface(new String[] {
                       PnPDBOperations.class.getName()}, null);

               c.add(createServiceDependency().setService(
                       IClusterGlobalServices.class).setCallbacks(
                       "setClusterGlobalServices",
                       "unsetClusterGlobalServices").setRequired(true));

               c.add(createServiceDependency().setService(
                       IConfigurationService.class).setCallbacks(
                       "setConfigurationService",
                       "unsetConfigurationService").setRequired(true));
               c.add(createServiceDependency().setService(
                       PnpServiceList.class).setCallbacks(
                       "setPnpServiceList",
                       "unsetPnpServiceList").setRequired(true));
               c.add(createServiceDependency().setService(
                       ICustomedConstructor.class).setCallbacks(
                       "setICustomedConstructor",
                       "unsetICustomedConstructor").setRequired(true));
           }
           if(imp.equals(PnPReflection.class)){
               c.setInterface(new String[] {
                       IPnPReflection.class.getName()}, null);
           }


       }
}

