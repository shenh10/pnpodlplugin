package org.opendaylight.controller.pnp.storage.api;

import java.io.Serializable;

import org.opendaylight.controller.configuration.ConfigurationObject;

public class Pair<T, P> extends ConfigurationObject implements Serializable{
    private T key;
    private P value;
    public Pair(T key,P value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(T value){

        this.key = value;

    }

    public T getKey(){

        return this.key;
    }
    public void setValue(P value){

        this.value = value;

    }

    public P getValue(){

        return this.value;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result= prime*result+((key == null) ? 0 : key.hashCode());

        result= prime*result+((value == null) ? 0 : value.hashCode());

        return result;
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false; } Pair other = (Pair) obj;
        if (key == null) {
            if (other.getKey() != null) {
                return false;
            }
        } else if (!key.equals(other.getKey())) {
            return false;
        }

        if (value == null) {
            if (other.getValue() != null) {
                return false;
            }
        } else if (!value.equals(other.getValue())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString(){
        return "[ key= :{" + key + "} value= :{" + value + "} }]"; }

}
