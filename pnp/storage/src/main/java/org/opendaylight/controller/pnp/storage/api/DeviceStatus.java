package org.opendaylight.controller.pnp.storage.api;

public enum DeviceStatus {
    ODL_PNP_INITIAL("Initial stage"),
    ODL_PNP_DEPLOYING("Deploying"),
    ODL_PNP_ERROR("Error"),
    ODL_PNP_FAULT("Fault"),
    ODL_PNP_DEPLOYED("Deployed"),
    ODL_PNP_WAITING_FOR_RELOAD("Waiting For Reload"),
    ODL_PNP_DEVICE_INFO_GOT("Device Info Got"),
    ODL_PNP_DAYTWO_NO_JOB("No Job Waiting"),
    ODL_PNP_DAYTWO_JOB_WAITING("Job Waiting"),
    ODL_PNP_DAYTWO_JOB_ERROR("Job Error"),
    ODL_PNP_DAYTWO_JOB_FAULT("Job Fault"),
    ODL_PNP_SERVER_INTERNAL_ERROR("Server Internal Error");
    private String status;
    private DeviceStatus(String status) {
        this.status = status;
    }
    public String getStatus() {
        return this.status;
    }
}
