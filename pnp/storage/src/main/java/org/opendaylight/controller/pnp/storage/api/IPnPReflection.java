package org.opendaylight.controller.pnp.storage.api;

import java.lang.reflect.InvocationTargetException;


public interface IPnPReflection {

    public Object hasReturnReflection(Object obj, String name)
            throws NoSuchMethodException, SecurityException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException;

    public  Boolean noReturnReflection(Object obj, String name, Class<?> cls, Object arg)
            throws NoSuchMethodException, SecurityException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException;


}
