package org.opendaylight.controller.pnp.storage.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;

import org.apache.felix.dm.Component;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.opendaylight.controller.clustering.services.CacheConfigException;
import org.opendaylight.controller.clustering.services.CacheExistException;
import org.opendaylight.controller.clustering.services.IClusterGlobalServices;
import org.opendaylight.controller.clustering.services.IClusterServices;
import org.opendaylight.controller.configuration.ConfigurationObject;
import org.opendaylight.controller.configuration.IConfigurationAware;
import org.opendaylight.controller.configuration.IConfigurationService;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.objects.api.ICustomedConstructor;
import org.opendaylight.controller.pnp.storage.api.DeviceKey;
import org.opendaylight.controller.pnp.storage.api.IMasterDB;
import org.opendaylight.controller.pnp.storage.api.Pair;
import org.opendaylight.controller.pnp.storage.api.PnPDBOperations;
import org.opendaylight.controller.pnp.storage.api.ServiceKey;
import org.opendaylight.controller.sal.utils.IObjectReader;
import org.opendaylight.controller.sal.utils.Status;
import org.opendaylight.controller.sal.utils.StatusCode;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PnPDB implements IObjectReader,
       PnPDBOperations,
       CommandProvider,
       IConfigurationAware{
           protected IClusterGlobalServices clusterGlobalService;
           protected IConfigurationService configurationService;
           protected static final Logger log = LoggerFactory.getLogger(PnPDB.class);
           protected static final String MASTERDB = "pnp.storage.masterDB";
           protected static final String DATADB = "pnp.storage.dataDB";
           protected static final String BULKDB = "pnp.storage.bulkDB";
           protected static final String DEFAULTDB = "pnp.storage.defaultDB";
           protected static final String DBPREFIX = "pnp.storage.";
           protected static final String FILE_NAME_POSTFIX = ".conf";
           protected Map<String,ConcurrentMap<ServiceKey, AssistantParent> > dataDB;
           protected ConcurrentMap<DeviceKey, IMasterDB> masterDB;
           protected ConcurrentMap<String, Object> defaultDB;
           protected ICustomedConstructor constructor;
           protected PnpServiceList serviceList;
           private Thread connectionEventThread;
           private BlockingQueue<PnPEvent> pnpEvents;
           void init(Component c) {
               allocateCaches();
               //         retrieveCaches();
               //connectionEventThread = new Thread(new EventHandler(),              "ConnectionEvent Thread");
               // this.pnpEvents = new LinkedBlockingQueue<PnPEvent>();
               String containerName = null;
               Dictionary<?, ?> props = c.getServiceProperties();
               if (props != null) {
                   containerName = (String) props.get("containerName");
               } else {
                   // In the Global instance case the containerName is empty
                   containerName = "UNKNOWN";
               }
               //registerWithOSGIConsole();
           }
           public void started() {

               registerWithOSGIConsole();
               loadConfiguration();
           }

           void stop() {
               log.info("Stopping bundle");
               saveConfiguration();
           }
           void stopped(){
               log.info("Bundle stopped");
           }
           void destroy() {
               destroyCaches();
           }
           @SuppressWarnings("unchecked")
           public void allocateCaches() {
               log.info("Start allocate caches:");
               this.masterDB = (ConcurrentMap<DeviceKey, IMasterDB>) allocateCache(
                       MASTERDB, EnumSet.of(IClusterServices.cacheMode.TRANSACTIONAL));
               this.dataDB = (Map<String , ConcurrentMap<ServiceKey, AssistantParent>>) allocateCache(
                       DATADB , EnumSet.of(IClusterServices.cacheMode.TRANSACTIONAL));
               this.defaultDB = (ConcurrentMap<String, Object>) allocateCache(
                       DEFAULTDB , EnumSet.of(IClusterServices.cacheMode.TRANSACTIONAL));
               Map<String, Class<?>> serviceDict = serviceList.getPnPAssistantDict();
               if(serviceDict == null){
                   log.error("serviceDict is null");
                   return;
               }
               for ( Map.Entry<String, Class<?>> entry : serviceDict.entrySet()) {
                   ConcurrentMap<ServiceKey, AssistantParent> tmpDB= (ConcurrentMap<ServiceKey, AssistantParent>) allocateCache(
                           DBPREFIX+entry.getKey(), EnumSet.of(IClusterServices.cacheMode.TRANSACTIONAL));
                   if(tmpDB == null){
                       log.error(entry.getKey()+ " cache allocate failed");
                   }
                   this.dataDB.put( entry.getKey() , tmpDB );
               }
           }
           private ConcurrentMap<?, ?> allocateCache(String cacheName,
                   Set<IClusterServices.cacheMode> cacheModes) {
               ConcurrentMap<?, ?> cache = null;
               log.info("Allocating cache: {}" , cacheName);
               try {
                   if(this.clusterGlobalService.existCache(cacheName)){
                       log.info("Cache {} exists, no need to be reallocated.", cacheName);
                       cache = this.clusterGlobalService.getCache(cacheName);
                   }else{
                       log.info("Cache {} created.", cacheName);
                       cache = this.clusterGlobalService.createCache(cacheName,
                               cacheModes);
                   }
               } catch (CacheExistException e) {
                   log.debug(cacheName
                           + " cache already exists - destroy and recreate if needed");
               } catch (CacheConfigException e) {
                   log.error(cacheName
                           + " cache configuration invalid - check cache mode");
               }
               if(cache ==null )
                   log.error("Cache allocate failed: {}" , cacheName);
               return cache;
           }
           public void destroyCaches() {
               log.info("Start destorying caches");
               this.clusterGlobalService.destroyCache(MASTERDB);
               this.masterDB = null;
               this.clusterGlobalService.destroyCache(DEFAULTDB);
               this.defaultDB = null;

               Map<String, Class<?>> serviceDict = serviceList.getPnPAssistantDict();
               if(serviceDict == null){
                   log.error("serviceDict is null");
                   return;
               }
               for ( Map.Entry<String, Class<?>> entry : serviceDict.entrySet()) {
                   this.clusterGlobalService.destroyCache(DBPREFIX+entry.getKey());
               }
               this.clusterGlobalService.destroyCache(DATADB);
               this.dataDB = null;

           }
           public void retrieveCaches() {
               log.info("Start retrieving caches:");
               if (this.clusterGlobalService == null) {
                   log.error("Cluster Services is null, can't retrieve caches.");
                   return;
               }

               this.masterDB = (ConcurrentMap<DeviceKey, IMasterDB>)this.clusterGlobalService.getCache(MASTERDB);
               if (this.masterDB == null) {
                   log.error("Failed to get cache for " + MASTERDB);
               }
               this.dataDB = (Map<String, ConcurrentMap<ServiceKey, AssistantParent>>)this.clusterGlobalService.getCache(DATADB);
               if (this.dataDB == null) {
                   log.error("Failed to get cache for " + DATADB);
               }
               Map<String, Class<?>> serviceDict = serviceList.getPnPDict();
               for ( Map.Entry<String, Class<?>> entry : serviceDict.entrySet()) {
                   ConcurrentMap<ServiceKey, AssistantParent> tmpDB= (ConcurrentMap<ServiceKey, AssistantParent>)
                       this.clusterGlobalService.getCache( DBPREFIX + entry.getKey());
                   this.dataDB.put( entry.getKey() , tmpDB );
               }
               this.defaultDB = (ConcurrentMap<String, Object>)this.clusterGlobalService.getCache(DEFAULTDB);
               if (this.defaultDB == null) {
                   log.error("Failed to get cache for " + DEFAULTDB);
               }
           }
           public Status saveConfigInternal() {
               log.info("Saving configurations!");
               Status saveStatus = null;
               //save masteDB to file
               saveStatus = saveConfigUnit(masterDB, MASTERDB+FILE_NAME_POSTFIX);
               if(!saveStatus.isSuccess()){
                   return saveStatus;
               }
               //save features' dbs to files

               for( Map.Entry<String, ConcurrentMap<ServiceKey, AssistantParent> > entry : dataDB.entrySet()){
                   String filename = DBPREFIX + entry.getKey()+FILE_NAME_POSTFIX;
                   saveStatus = saveConfigUnit(entry.getValue(), filename);
                   if (!saveStatus.isSuccess()) {
                       return saveStatus;
                   }
               }
               //save defaultDB to file
               saveStatus = saveConfigUnit(defaultDB, DEFAULTDB+FILE_NAME_POSTFIX);
               if(!saveStatus.isSuccess())
                   return saveStatus;
               return saveStatus;
           }
           private <T1,T2> Status saveConfigUnit(Map<T1,T2> db ,String filename){
               Status saveStatus = null;
               for( Map.Entry<T1,T2 > entry : db.entrySet()){
                   List<Pair<T1, T2>> list = new  ArrayList<>();
                   list.add(new Pair<T1, T2>(entry.getKey(), entry.getValue()));
                   saveStatus = this.configurationService.persistConfiguration(
                           (List<ConfigurationObject>)(List<?>)list,filename);
                   if (!saveStatus.isSuccess()) {
                       log.error(entry.getKey()+" save to file"+filename+" failed: " + saveStatus.getDescription());
                       return new Status(StatusCode.INTERNALERROR,
                               entry.getKey()+" save to file"+filename+" failed: " + saveStatus.getDescription());
                   }
               }
               return new Status(StatusCode.SUCCESS);
           }
           private void loadConfiguration() {
               log.info("Loading configuration");
               for (ConfigurationObject conf : configurationService.retrieveConfiguration(this, MASTERDB + FILE_NAME_POSTFIX)) {
                   Pair<DeviceKey, IMasterDB> entry = (Pair<DeviceKey, IMasterDB>)conf;
                   masterDB.putIfAbsent(entry.getKey(), entry.getValue());
               }
               Map<String, Class<?>> assistantDict = serviceList.getPnPAssistantDict();
               for ( Map.Entry<String, Class<?>> entry : assistantDict.entrySet()) {
                   for (ConfigurationObject conf : configurationService.retrieveConfiguration(this, DBPREFIX+ entry.getKey() + FILE_NAME_POSTFIX)) {
                       Pair<ServiceKey, AssistantParent> obj = (Pair<ServiceKey, AssistantParent>)conf;
                       ConcurrentMap<ServiceKey, AssistantParent>  featureMap= dataDB.get(entry.getKey()) ;
                       featureMap.putIfAbsent(obj.getKey(), obj.getValue());
                   }
               }

               for (ConfigurationObject conf : configurationService.retrieveConfiguration(this, DEFAULTDB + FILE_NAME_POSTFIX)) {
                   Pair<String, Object> entry = (Pair<String,Object>)conf;
                   defaultDB.putIfAbsent(entry.getKey(), entry.getValue());
               }
           }
           @Override
           public Object readObject(ObjectInputStream ois)
           throws FileNotFoundException, IOException, ClassNotFoundException {
           return ois.readObject();
           }
           @Override
           public String getHelp() {
               StringBuffer help = new StringBuffer();
               help.append("---PnPDB---\n");
               help.append("\t addUserLink <name> <node connector string> <node connector string>\n");
               help.append("\t deleteUserLink <name>\n");
               help.append("\t printUserLink\n");
               help.append("\t printNodeEdges\n");
               return help.toString();
           }
           private void registerWithOSGIConsole() {
               BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass())
                   .getBundleContext();
               bundleContext.registerService(CommandProvider.class.getName(), this,
                       null);
           }
           void setClusterGlobalServices(IClusterGlobalServices s) {
               log.debug("Cluster Service set");
               this.clusterGlobalService = s;
           }

           void unsetClusterGlobalServices(IClusterGlobalServices s) {
               if (this.clusterGlobalService == s) {
                   log.debug("Cluster Service removed!");
                   this.clusterGlobalService = null;
               }
           }

           public void setConfigurationService(
                   IConfigurationService service) {
               log.trace("Got configuration service set request {}", service);
               this.configurationService = service;
                   }

           public void unsetConfigurationService(
                   IConfigurationService service) {
               log.trace("Got configuration service UNset request");
               this.configurationService = null;
                   }
           public void setPnpServiceList(
                   PnpServiceList service) {
               log.trace("Got PnpServiceList  set request {}", service);
               this.serviceList = service;
                   }

           public void unsetPnpServiceList(
                   PnpServiceList service) {
               log.trace("Got PnpServiceList service UNset request");
               this.serviceList = null;
                   }
           public void setICustomedConstructor(
                   ICustomedConstructor service) {
               log.trace("Got ICustomedConstructor  set request {}", service);
               this.constructor = service;
                   }

           public void unsetICustomedConstructor(
                   ICustomedConstructor service) {
               log.trace("Got ICustomedConstructor service UNset request");
               this.constructor = null;
                   }
           @Override
           public Status saveConfiguration() {
               return saveConfigInternal();
           }
           private Boolean validateType(Object key, Class<?> cls){
               if(!key.getClass().equals(cls)){
                   log.error("Type error: object should be instance of "+cls.getName());
                   return false;
               }
               return true;
           }
           @Override
           public Object entryLookUp(final Object key, final String cacheName){
               if(key == null) return null;
               log.info("Looking up cache {} ...",cacheName);
               if (cacheName.equals(MASTERDB)) {
                   // This is the case of an Edge being added to the topology DB
                   if(!validateType(key, DeviceKey.class)){
                       return null;
                   }
                   final DeviceKey e = (DeviceKey) key;
                   return masterDB.get(key);
               }

               else if(cacheName.equals(DEFAULTDB)){
                   if(!validateType(key, String.class)){
                       return null;
                   }
                   return defaultDB.get(key);
               }else{

                   for(Map.Entry<String, ConcurrentMap<ServiceKey, AssistantParent> > entry : dataDB.entrySet()){
                       if(cacheName.equals(DBPREFIX+ entry.getKey())){
                           ConcurrentMap<ServiceKey, AssistantParent> tmp = entry.getValue();
                           if(!validateType(key, ServiceKey.class)){
                               return null;
                           }
                           return tmp.get(key);

                       }
                   }
               }
               log.error("CacheName {} not exists!",cacheName);
               return null;
           }
           @Override
           public Boolean entryCreate(final Object key, final Object value, final String cacheName) {
               if(key == null) return false;
               if (cacheName.equals(MASTERDB)) {
                   // This is the case of an Edge being added to the topology DB
                   if(!validateType(key, DeviceKey.class)){
                       return null;
                   }
                   final DeviceKey e = (DeviceKey) key;
                   if(value != null){
                       log.trace("MasterDB {} CREATED :{}",e , (IMasterDB)value);
                       masterDB.putIfAbsent(e, (IMasterDB)value);
                       log.info("Entry created in {}", "masterDB");
                   }else
                   {
                       log.trace("MasterDB {} CREATED :{}",e , null);
                       masterDB.putIfAbsent(e,null);
                   }
               }
               else if(cacheName.equals(DEFAULTDB)){
                   if(!validateType(key, String.class)){
                       return null;
                   }
                   defaultDB.putIfAbsent((String)key, value);
                   log.info("Entry Created in {}", DEFAULTDB);
               }else{
                   for(Map.Entry<String, ConcurrentMap<ServiceKey, AssistantParent> > entry : dataDB.entrySet()){
                       if(cacheName.equals(DBPREFIX+ entry.getKey())){
                           ConcurrentMap<ServiceKey, AssistantParent> tmp = entry.getValue();
                           if(!validateType(key, ServiceKey.class)){
                               return null;
                           }
                           if(value != null)
                           {
                               tmp.putIfAbsent((ServiceKey)key, (AssistantParent)value);
                           }
                           else{
                               tmp.putIfAbsent((ServiceKey)key, null);
                           }
                           log.info("Entry Created in {}",DBPREFIX+ entry.getKey());
                       }
                   }
               }
               return true;
           }

           @Override
           public Boolean entryUpdate(final Object key, final Object new_value,
                   final String cacheName) {
               if(key == null) return false;
               if (cacheName.equals(MASTERDB)) {
                   // This is the case of an Edge being added to the topology DB
                   if(!validateType(key, DeviceKey.class)){
                       return null;
                   }
                   DeviceKey e = (DeviceKey) key;
                   if(masterDB.containsKey(e)){
                       IMasterDB v = (IMasterDB) masterDB.get(e);
                       masterDB.replace(e,v , (IMasterDB)new_value);
                       log.info("MasterDB {} Updated :{}",e , (IMasterDB)new_value);
                   }
                   else
                       entryCreate(key,new_value, cacheName );
               }
               else if(cacheName.equals(DEFAULTDB)){
                   if(!validateType(key, String.class)){
                       return null;
                   }
                   if(defaultDB.containsKey(key)){
                       Object dkey = defaultDB.get((String)key);
                       defaultDB.replace((String) key, dkey,new_value);
                       log.info("SessionDB {} updated :{}",key , new_value);
                   }else
                       entryCreate(key,new_value, cacheName );
               }
               else{
                   for(Map.Entry<String, ConcurrentMap<ServiceKey, AssistantParent> > entry : dataDB.entrySet()){
                       if(cacheName.equals(DBPREFIX+ entry.getKey())){
                           ConcurrentMap<ServiceKey, AssistantParent> tmp = entry.getValue();
                           if(!validateType(key, ServiceKey.class)){
                               return null;
                           }
                           if(tmp.containsKey(key)){
                               AssistantParent obj = tmp.get(key);
                               if(new_value != null)
                                   tmp.replace((ServiceKey) key , obj ,(AssistantParent)new_value);
                               else
                                   tmp.replace((ServiceKey) key , obj ,null);
                               log.info("{}DB {} Updated :{}", cacheName ,key , new_value);
                           }else
                               entryCreate(key,new_value, cacheName );
                       }
                   }
               }
               return true;
           }

           @Override
           public Boolean entryDelete(final Object key, final String cacheName) {
               if(key == null) return false;
               if (cacheName.equals(MASTERDB)) {
                   // This is the case of an Edge being added to the topology DB
                   if(!validateType(key, DeviceKey.class)){
                       return null;
                   }
                   if(masterDB.containsKey(key)){
                       masterDB.remove(key);
                       log.info("MasterDB {} Deleted ",key);
                   }
               }
               else if(cacheName.equals(DEFAULTDB)){
                   if(!validateType(key, String.class)){
                       return null;
                   }
                   if(defaultDB.containsKey(key)){
                       defaultDB.remove(key);
                       log.info("defaultDB {} Deleted ",key);
                   }
               }else{
                   for(Map.Entry<String, ConcurrentMap<ServiceKey, AssistantParent> > entry : dataDB.entrySet()){
                       if(cacheName.equals(DBPREFIX+ entry.getKey())){
                           ConcurrentMap<ServiceKey, AssistantParent> tmp = entry.getValue();
                           if(!validateType(key, ServiceKey.class)){
                               return null;
                           }
                           if(tmp.containsKey(key)){
                               tmp.remove(key);
                               log.info("{}DB {} Deleted ", cacheName ,key);
                           }
                       }
                   }
               }
               return true;
           }
           @Override
           public Boolean entryDeleteByValue(final Object value, final String cacheName){
               if (cacheName.equals(MASTERDB)) {
                   // This is the case of an Edge being added to the topology DB
                   if(value!=null && !value.getClass().equals(IMasterDB.class)){
                       log.error("Type error: value should be instance of "+IMasterDB.class.getName());
                       return false;
                   }
                   if(masterDB.containsValue(value)){
                       for(Map.Entry<DeviceKey, IMasterDB> entry: masterDB.entrySet()){
                           if(entry.getValue().equals(value)){
                               masterDB.remove(entry.getKey());
                               log.info("MasterDB {} Deleted ",entry.getKey());
                           }
                       }
                   }
               }
               else if(cacheName.equals(DEFAULTDB)){
                   if(defaultDB.containsValue(value)){
                       for(Map.Entry<String , Object> entry: defaultDB.entrySet()){
                           if(entry.getValue().equals(value)){
                               defaultDB.remove(entry.getKey());
                               log.info("defaultDB {} Deleted ",entry.getKey());
                           }
                       }
                   }
               }
               else {
                   for(Map.Entry<String, ConcurrentMap<ServiceKey, AssistantParent> > entry : dataDB.entrySet()){
                       if(cacheName.equals(DBPREFIX+ entry.getKey())){
                           ConcurrentMap<ServiceKey, AssistantParent> tmp = entry.getValue();
                           if(value!=null && !value.getClass().equals(AssistantParent.class)){
                               log.error("Type error: value should be instance of "+AssistantParent.class.getName());
                               return false;
                           }
                           if(tmp.containsValue(value)){
                               for(Map.Entry<ServiceKey, AssistantParent> subentry: tmp.entrySet()){
                                   if(subentry.getValue().equals(value)){
                                       tmp.remove(subentry.getKey());
                                       log.trace("{}DB {} Deleted ",cacheName,subentry.getKey());
                                   }
                               }
                           }
                       }
                   }
               }
               return true;
           }


           @Override
           public  List<?> getTable(String cacheName){
               if (cacheName.equals(MASTERDB)) {
                   List<IMasterDB> list = new ArrayList<>();
                   for(Map.Entry<DeviceKey, IMasterDB> entry: masterDB.entrySet()){
                       list.add(entry.getValue());
                   }
                   return list;
               }
               return null;
           }

}
