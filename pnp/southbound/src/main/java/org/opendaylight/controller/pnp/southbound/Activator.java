package org.opendaylight.controller.pnp.southbound;

import org.apache.felix.dm.Component;
//import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
//import org.opendaylight.controller.pnp.objects.api.PnpXmlHandler;
//import org.opendaylight.controller.pnp.objects.api.ICustomedConstructor;
//import org.opendaylight.controller.pnp.storage.api.PnPDBOperations;
import org.opendaylight.controller.sal.core.ComponentActivatorAbstractBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Activator extends ComponentActivatorAbstractBase{
    protected static final Logger logger = LoggerFactory
               .getLogger(Activator.class);
    @Override
    public Object[] getImplementations() {
      return new Object[]{};
   }
   @Override
    public void configureInstance(Component c, Object imp, String containerName) {
    }
   @Override
    protected Object[] getGlobalImplementations() {
       Object[] res = { PnPSouthboundImpl.class, PnPSouthboundJAXRS.class};
        return res;
    }

      @Override
      protected void configureGlobalInstance(Component c, Object imp) {
           if (imp.equals(PnPSouthboundImpl.class)) {
               logger.info("PnPSouthboundImpl invoked");
//           c.add(createServiceDependency().setService(
//                  PnpXmlHandler.class).setCallbacks(
//                   "setPnpXmlHandler",
//                   "unsetPnpXmlHandler").setRequired(true));
//           c.add(createServiceDependency().setService(
//                   PnPDBOperations.class).setCallbacks(
//                   "setPnPDBOperations",
//                   "unsetPnPDBOperations").setRequired(true));
//           c.add(createServiceDependency().setService(
//                   PnpServiceList.class).setCallbacks(
//                   "setPnpServiceList",
//                   "unsetPnpServiceList").setRequired(true));
//           c.add(createServiceDependency().setService(
//                   ICustomedConstructor.class).setCallbacks(
//                   "setICustomedConstructor",
//                   "unsetICustomedConstructor").setRequired(true));
//           System.out.println("End configure PnpImpl Global instance.");
           }
           if(imp.equals(PnPSouthboundJAXRS.class)){
               logger.info("PnPSouthboundJAXRS invoked");
           }
       }
}

