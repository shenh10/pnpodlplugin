package org.opendaylight.controller.pnp.southbound;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.apache.felix.dm.Component;
import org.opendaylight.controller.northbound.commons.RestMessages;
import org.opendaylight.controller.northbound.commons.exception.ResourceNotFoundException;
import org.opendaylight.controller.pnp.objects.api.AssistantParent;
import org.opendaylight.controller.pnp.objects.api.PnpServiceList;
import org.opendaylight.controller.pnp.objects.api.PnpXmlHandler;
import org.opendaylight.controller.pnp.objects.api.ServiceEnum;
import org.opendaylight.controller.pnp.storage.api.DeviceKey;
import org.opendaylight.controller.pnp.storage.api.DeviceStatus;
import org.opendaylight.controller.pnp.objects.api.ICustomedConstructor;
import org.opendaylight.controller.pnp.storage.api.IMasterDB;
import org.opendaylight.controller.pnp.storage.api.IPnPReflection;
import org.opendaylight.controller.pnp.storage.api.PnPDBOperations;
import org.opendaylight.controller.pnp.storage.api.ServiceKey;
import org.opendaylight.controller.sal.utils.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class PnPSouthboundImpl    {
    private static final Logger log = LoggerFactory.getLogger(PnPSouthboundImpl.class);
    protected static final String MASTERDB = "pnp.storage.masterDB";
    protected static final String SESSIONDB = "pnp.storage.sessionDB";
    protected static final String DEFAULTDB = "pnp.storage.defaultDB";
    protected static final String DBPREFIX = "pnp.storage.";
    protected static final String BULKDB = "pnp.storage.bulkDB";
    protected static final Integer MAXNOCHECKTIME=180;
    protected static final String  POSTRELOADPREV="ztd";
    protected PnpXmlHandler xmlhandler;
    protected PnpServiceList services;
    protected PnPDBOperations dbs;
    protected ICustomedConstructor constructor;
    protected IPnPReflection ref;
    public PnPSouthboundImpl(){
        this.xmlhandler = (PnpXmlHandler) ServiceHelper.getGlobalInstance(PnpXmlHandler.class,
                this);
        if (this.xmlhandler == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
        this.services = (PnpServiceList) ServiceHelper.getGlobalInstance(PnpServiceList.class,
                this);
        if (this.services == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
        this.dbs = (PnPDBOperations) ServiceHelper.getGlobalInstance(PnPDBOperations.class,
                this);
        if (this.dbs == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
        this.constructor = (ICustomedConstructor) ServiceHelper.getGlobalInstance(ICustomedConstructor.class,
                this);
        if (this.constructor == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }

        this.ref = (IPnPReflection) ServiceHelper.getGlobalInstance(IPnPReflection.class,
                this);
        if (this.ref == null) {
            throw new ResourceNotFoundException(RestMessages.NOCONTAINER.toString());
        }
    }
    void setPnpXmlHandler(PnpXmlHandler s) {
        log.info("PnpXmlHandler Service set");
        this.xmlhandler = s;
    }

    void unsetPnpXmlHandler(PnpXmlHandler s) {
        if (this.xmlhandler == s) {
            log.info("PnpXmlHandler Service removed!");
            this.xmlhandler = null;
        }
    }
    void setPnPDBOperations(PnPDBOperations s) {
        log.info("PnPDBOperations Service set");
        this.dbs = s;
    }

    void unsetPnPDBOperations(PnPDBOperations s) {
        if (this.dbs == s) {
            log.info("PnPDBOperations Service removed!");
            this.dbs = null;
        }
    }
    void setPnpServiceList(PnpServiceList s) {
        log.info("PnpServiceList Service set");
        this.services = s;
    }

    void unsetPnpServiceList(PnpServiceList s) {
        if (this.services == s) {
            log.info("PnpServiceList Service removed!");
            this.services = null;
        }
    }
    void setICustomedConstructor(ICustomedConstructor s) {
        log.info("ICustomedConstructor Service set");
        this.constructor = s;
    }

    void unsetICustomedConstructor(ICustomedConstructor s) {
        if (this.constructor == s) {
            log.info("ICustomedConstructor Service removed!");
            this.constructor = null;
        }
    }
    void init(Component c) {
        log.info("Start bundle initialization.");
    }
    public String RequestProcess(String request, Boolean isWorkRequest) throws Exception{
        log.info("Start handling request {}", request);
        String response = null;
        String service = this.xmlhandler.parseService(request);
        Object obj = null;
        if(!services.containsService(service)){
            log.error("Service {} not supported by PnP:" + service);
            return null;
        }else if(service.equals(ServiceEnum.ODL_PNP_WORKINFO.getService()) && isWorkRequest){
            obj = this.xmlhandler.xmlParser(request, ServiceEnum.ODL_PNP_DEVICEID.getService());
        }
        else obj = this.xmlhandler.xmlParser(request, service);
        if(service.equals(ServiceEnum.ODL_PNP_WORKINFO.getService())){
            if(isWorkRequest)
                response = parseDeviceId(obj);
        }
        else {
            response = parseFeature(service, obj);
        }
        log.info("Request Process done. Response back...");
        return response;
    }

    public String parseDeviceId(Object object) throws Exception{
        log.info("Parse Device ID...");
        Object info = ref.hasReturnReflection(object, "getInfo");
        if(info == null){
            log.error("No {} content in {}.","info" , object);
            return null;
        }
        Object devId = ref.hasReturnReflection(info,"getDeviceId");
        if(devId == null){
            log.error("No {} content in {}.", "deviceId", devId);
            return null;
        }
        Object udiObj = ref.hasReturnReflection(devId, "getUdi");
        if(udiObj == null){
            log.error("No {} content in {}.","udi", devId);
            return null;
        }
        String udi = (String) udiObj;
        DeviceKey dkey = new DeviceKey();
        if(!dkey.parseUdi(udi)){
            log.error("UDI {} is in wrong format!", udi);
            return null;
        }
        Object lookupres = dbs.entryLookUp(dkey, MASTERDB);
        if(lookupres!=null){
            log.info("Device {} exists in MasterDB", udi);
            IMasterDB props = (IMasterDB)lookupres;
            if(!props.parseEntry(object)){
                log.error("Fail to update entry for {}!", object);
                return null;
            }
            log.info("Parsed Entry {}", props);
            // Device in error state, no matter in day1 or day2, send backoff
            if(props.getStatus().equals(DeviceStatus.ODL_PNP_ERROR)
                    || props.getStatus().equals(DeviceStatus.ODL_PNP_FAULT)
                    || props.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_ERROR)
                    || props.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_FAULT)){
                log.info("Device in error state. It might be caused by last feature's deployment failure");
                return sendBackoff(dkey,props);
                // Device in day1
            }else if(!props.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)){
                // Device in day 0 , initial state, send device info request
                if(props.getStatus().equals(DeviceStatus.ODL_PNP_INITIAL)){
                    log.info("Request for device info");
                    return sendDeviceInfo(dkey,object,props);
                }else if(props.getSequence()!=null
                        && props.getNext() >= 0 && props.getNext()<props.getSequence().size()){
                    return sendNextService(props, dkey);
                }else
                {
                    //send backoff
                    log.info("No feature configured/All features implemented, backoff...");
                    return sendBackoff(dkey,props);
                }
            }else{
                if(props.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_WAITING)
                        && props.getDynamicSequence()!=null && !props.getDynamicSequence().isEmpty() ){
                    return sendNextService(props, dkey);
                }else
                {
                    //send backoff
                    log.info("No feature configured/All features implemented, backoff...");
                    return sendBackoff(dkey,props);
                }
            }
        }
        log.info("Device {} not exists yet, add into database...", udi);
        //add new device into MasterDB
        IMasterDB props_new = new IMasterDB();
        if(!props_new.parseEntry(object)){
            log.error("Fail to construct to entry for {}!", object);
            return null;
        }
        dbs.entryCreate(dkey, props_new, MASTERDB);
        //get device-info object
        log.info("Device {} successfully added .", udi);
        return sendDeviceInfo(dkey,object,props_new);
    }
    public String parseFeature(String service, Object object) throws Exception{
        log.info("Parse feature response...");
        //get Correlator
        if(object == null ){
            log.error("Feature Object given is null");
            return null;
        }
        if(service == null){
            log.error("Service given for {} is null", object);
            return null ;
        }
        Object res = getRequestOrResponse(object);
        if(res == null){
            log.error("Response has no <response> tag ,invalid: {} ", object);
            return null;
        }
        String udi = getHeadUdi(object);
        if( udi == null ){
            log.error("No udi found in feature {}, invaild", object);
            return null;
        }
        DeviceKey dkey = new DeviceKey();
        if(!dkey.parseUdi(udi)){
            log.error("UDI {} is in wrong format!", udi);
            return null;
        }
        Object props_obj = dbs.entryLookUp(dkey, MASTERDB);
        IMasterDB props = null;
        if(props_obj != null)
            props = (IMasterDB) props_obj;
        else{
            log.error("No device with UDI {} found in database", udi);
            return null;
        }
        //if service is pnp_fault, handle first since the feature fault doesn't have success flag
        if(service.equals(ServiceEnum.ODL_PNP_FAULT.getService())){
            log.info("Pnp fault recieved!");
            ServiceKey skey = null;
            props.setFaultFlag(true);
            //day2 fault
            if(props.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)){
                log.info("Day2 fault detected, update status...");
                skey = new ServiceKey(dkey,0,ServiceEnum.ODL_PNP_FAULT.getService(), 1);
                props.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_JOB_FAULT);
            }
            //day1 fault
            else{
                log.info("Day1 fault detected, update status...");
                skey = new ServiceKey(dkey,0,ServiceEnum.ODL_PNP_FAULT.getService(), 0);
                props.setStatus(DeviceStatus.ODL_PNP_FAULT);
            }
            if(skey == null){
                return null;
            }
            //search whether fault info exist in pnp.storage.fault
            Object entry = dbs.entryLookUp(skey, DBPREFIX+ ServiceEnum.ODL_PNP_FAULT.getService());
            // Parse current fault response
            AssistantParent ap= (AssistantParent) services.getAssistant(ServiceEnum.ODL_PNP_FAULT.getService()).newInstance();
            ap.fromFeature(object);
            //if not exist, create a new entry in pnp.storage.fault
            if(entry == null ){
                log.info("No feature {} of device {} found in database {}, add this one in.", ServiceEnum.ODL_PNP_FAULT.getService(), udi,    DBPREFIX+ ServiceEnum.ODL_PNP_FAULT.getService());
                dbs.entryCreate(skey, ap, DBPREFIX + ServiceEnum.ODL_PNP_FAULT.getService());
            }else {
                //if  exist, update the entry in pnp.storage.fault
                log.info("Update feature {} for device {}", ServiceEnum.ODL_PNP_FAULT.getService(), udi);
                dbs.entryUpdate(skey, ap, DBPREFIX + ServiceEnum.ODL_PNP_FAULT.getService());
            }
            log.info("Critial fault from device side: {}...", ap);
            //send workinfo back, finish handshake this round
            return sendWorkInfo(props);
        }
        String correlator = getCorrelator(res);
        if( correlator == null){
            log.error("No correlator defined, session unknown: {}", object);
            return null;
        }
        log.info("Validate correlator {}",correlator);
        if(!validateCorrelator(correlator, props)){
            log.error("Correlator {} for device {} not matched!", correlator, udi);
            return null;
        }
        log.info("Correlator validated!");
        // For other response, check "success" attribute value
        log.info("Checking 'success' attribute...");
        Object resultObj = ref.hasReturnReflection(res,"isSuccess");
        if(resultObj == null){
            log.error("No success element defined in {}", res);
            return null;
        }
        boolean success = (boolean)resultObj;
        //Check response
        if(success == false){
            //populate masterDB with errors , update status
            if(!updateErrorInfo(props, res,service)){
                log.error("Update error info failed for entry {}!", props);
                props.setStatus(DeviceStatus.ODL_PNP_ERROR);
                return null;
            }

            log.info("Previous feature of device {} failed to be executed", udi);
            //sendBackoff
            return sendWorkInfo(props);
        }
        //if success, check whether it is "device-info" feature
        //Seperately handle "device-info" because the db stores the response info instead of request info
        if(service.equals(ServiceEnum.ODL_PNP_DEVICEINFO.getService())){
            log.info("device-info response recieved.");
            ServiceKey skey = null;
            skey = new ServiceKey(dkey,0,ServiceEnum.ODL_PNP_DEVICEINFO.getService(), 0);
            Object entry = dbs.entryLookUp(skey, DBPREFIX+ ServiceEnum.ODL_PNP_DEVICEINFO.getService());
            AssistantParent ap= (AssistantParent) services.getAssistant(ServiceEnum.ODL_PNP_DEVICEINFO.getService()).newInstance();
            ap.fromFeature(object);
            if(entry == null ){
                log.info("No feature {} of device {} found in database {}, add this one in.", ServiceEnum.ODL_PNP_DEVICEINFO.getService(), udi,    DBPREFIX+ ServiceEnum.ODL_PNP_DEVICEINFO.getService());
                dbs.entryCreate(skey, ap    , DBPREFIX + ServiceEnum.ODL_PNP_DEVICEINFO.getService());
            }else {
                log.info("Update feature {} for device {}", ServiceEnum.ODL_PNP_DEVICEINFO.getService(), udi);
                dbs.entryUpdate(skey, ap, DBPREFIX + ServiceEnum.ODL_PNP_DEVICEINFO.getService());
            }
            if(props.getSequence()!=null && (props.getSequence().get(props.getNext())).equals(ServiceEnum.ODL_PNP_DEVICEINFO.getService())){
                props.setNext(props.getNext()+1);
            }else
                props.setStatus(DeviceStatus.ODL_PNP_DEVICE_INFO_GOT);
        }// if not "device-info", handle it depending on day1 and day2
        else{
            //day2 feature, remove from dynamic sequence and push it into history sequence
            if(props.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)){
                log.info("Day2 feature successfully deployed, update DB...");
                if(!moveDynamicSequence(props)){
                    log.error("Day2: Move sequence failed! ");
                    props.setStatus(DeviceStatus.ODL_PNP_SERVER_INTERNAL_ERROR);
                }
                if(props.getDynamicSequence().isEmpty())
                    props.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_NO_JOB);
            }else{
                //day1 feature, move pointer, update status if all features are deployed
                if(props.getStatus().equals(DeviceStatus.ODL_PNP_WAITING_FOR_RELOAD)){
                    log.info("Reload success! Reset device to initial stage");
                     props.setStatus(DeviceStatus.ODL_PNP_INITIAL);
                     props.setSequence(null);
                     ServiceKey skey = new ServiceKey(dkey, 0, ServiceEnum.ODL_PNP_RELOAD.getService(), 0);
                     if(!dbs.entryDelete(skey, DBPREFIX+ServiceEnum.ODL_PNP_RELOAD.getService())){
                         log.error("Failed to delete reload entry");
                     }
                }else{
                log.info("Day1 feature successfully deployed, update DB...");
                if(props.getSequence()!=null && props.getSequence().size()> 0 &&
                        !props.getStatus().equals(DeviceStatus.ODL_PNP_ERROR)
                        && !props.getStatus().equals(DeviceStatus.ODL_PNP_FAULT))
                    props.setNext(props.getNext()+1);
                if(props.getSequence()!=null && props.getSequence().size()> 0 && props.getNext()== props.getSequence().size()){
                    log.info("All features configured for device {} are executed.", udi);
                    props.setStatus(DeviceStatus.ODL_PNP_DEPLOYED);
                }
            }
            }
        }
        //Send work-info back to close handshake this round
        return sendWorkInfo(props);

    }
    /*change step for waiting task in dynamic sequence
     * push deployed one into history sequence
     */
    public Boolean moveDynamicSequence(IMasterDB props){
        DeviceKey dkey = props.getDkey();
        if(props.getHistorySequence() == null)
            props.setHistorySequence(new ArrayList<String>());
        ArrayList<String> features = props.getDynamicSequence();
        for(int i=0; i< features.size(); i++){
            ServiceKey skey = new ServiceKey(dkey, i, features.get(i), 1);
            Object res_f = dbs.entryLookUp(skey, DBPREFIX+features.get(i));
            if(res_f != null){
                if(!dbs.entryDelete(skey, DBPREFIX+features.get(i))){
                    log.error("Day2: Delete feature {} in corresponding db failed",features.get(i));
                    return false;
                }
                if(i==0)
                {
                    skey.setStep(props.getHistorySequence().size());
                    skey.setDynamicFlag(2);
                }else
                    skey.setStep(i-1);
                if(!dbs.entryCreate(skey, res_f, DBPREFIX+features.get(i))){
                    log.error("Day2: Create feature {} in corresponding db failed",features.get(i));
                    return false;
                }
            }
        }
        props.getHistorySequence().add(props.getDynamicSequence().get(0));
        props.getDynamicSequence().remove(0);
        return true;
    }
    public String sendDeviceInfo(DeviceKey dkey,Object object, IMasterDB entry) throws Exception{
        Object obj_s = constructor.newInstance(services.getService(ServiceEnum.ODL_PNP_DEVICEINFO.getService()));
        if(obj_s == null){
            return null;
        }
        Object req_s = getRequestOrResponse(obj_s);
        if(req_s == null)
        {
            return null;
        }
        Object devInfo =ref.hasReturnReflection(req_s,"getDeviceInfo");
        if(devInfo == null)
        {
            log.error("No {} content in {}.","deviceInfo", req_s);
            return null;
        }
        String correlator = entry.getCorrelator();
        if(correlator == null)
        {
            log.error("No correlator for device {}, something wrong!", entry.getDkey().toUdi());
            return null;
        }
        if(!ref.noReturnReflection(devInfo,"setType",String.class,"all")){
            log.error("{} API call failed for {}" ,"setType",devInfo);
        }
        setCommonInfo(obj_s, req_s, entry, correlator, ServiceEnum.ODL_PNP_DEVICEINFO.getService());
        return xmlhandler.xmlGenerator(obj_s);
    }
    public String sendNextService(IMasterDB entry, DeviceKey dkey) throws Exception{
        String ser = null;
        ServiceKey skey = null;
        if(entry.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)){
            ser = (entry.getDynamicSequence()).get(0);
            if(ser == null)
            {
                log.error("No service to be executed for device {}: {}", dkey, entry);
                return null;
            }
            skey = new ServiceKey(dkey,0 ,ser, 1);

        }else{
            ser = (entry.getSequence()).get(entry.getNext());
            if(ser == null)
            {
                log.error("No service to be executed for device {}: {}", dkey, entry);
                return null;
            }
            skey = new ServiceKey(dkey,entry.getNext() ,ser, 0);
        }
        Object obj_s = dbs.entryLookUp(skey, DBPREFIX+ ser);
        log.info("Look up feature db done");
        //getCorrelator
        String correlator = entry.getCorrelator();
        if(obj_s == null)
        {
            sendBackoff(dkey,entry);
            log.error("Service configuration not exists in database {}, sending backoff...!" ,    DBPREFIX+ ser);
            return null;
        }
        Object obj_send = ((AssistantParent)obj_s).toFeature();
        log.info("object to be sent:{}", obj_send);
        setCommonInfo(obj_send, getRequestOrResponse(obj_send), entry, correlator, ser);
        log.info("set common info successfully");
        //push new request correlator to session table
        //update Next
        if(!entry.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)
                && !entry.getStatus().equals(DeviceStatus.ODL_PNP_WAITING_FOR_RELOAD))
            entry.setStatus(DeviceStatus.ODL_PNP_DEPLOYING);
        return xmlhandler.xmlGenerator(obj_send);
    }
    public String sendBackoff(DeviceKey dkey, IMasterDB entry) throws Exception{
        log.info("Sending backoff...");
        String reason = "";
        ServiceKey skey = null;
        Object obj_s = null;
        // Error happen
        if(entry.getStatus().equals(DeviceStatus.ODL_PNP_ERROR)
                || entry.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_ERROR)
                || entry.getStatus().equals(DeviceStatus.ODL_PNP_FAULT)
                || entry.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_JOB_FAULT)){
            reason = "Device in error status. Please clear up server status before deploying upcoming features ";
                }// No more feature to be deploy
        else if( (entry.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED)
                    || entry.getStatus().equals(DeviceStatus.ODL_PNP_DEVICE_INFO_GOT))
                && entry.getDynamicStatus().equals(DeviceStatus.ODL_PNP_DAYTWO_NO_JOB))
        {
            reason = "No service to be executed.";
            // Configured by user as day1 feature
        }else if(entry.getSequence().get(entry.getNext()).equals(ServiceEnum.ODL_PNP_BACKOFF.getService())
            ){
            skey = new ServiceKey(dkey, entry.getNext(),ServiceEnum.ODL_PNP_BACKOFF.getService(),0 );
            obj_s = dbs.entryLookUp(skey, DBPREFIX+ ServiceEnum.ODL_PNP_BACKOFF.getService());
            // Configured by user as day2 feature
        }else if( entry.getDynamicSequence().get(0).equals(ServiceEnum.ODL_PNP_BACKOFF.getService())){
            skey = new ServiceKey(dkey, 0,ServiceEnum.ODL_PNP_BACKOFF.getService(),1 );
            obj_s = dbs.entryLookUp(skey, DBPREFIX+ ServiceEnum.ODL_PNP_BACKOFF.getService());
        }

        if(obj_s == null){
            log.info("No backoff setting for device {}, look up default setting instead", dkey);
            obj_s = dbs.entryLookUp(ServiceEnum.ODL_PNP_BACKOFF.getService(), DEFAULTDB);
        }
        if(obj_s == null){
            log.info("No backoff default setting, hard coding one (Not recommend)");
            obj_s = constructor.newInstance(services.getAssistant(ServiceEnum.ODL_PNP_BACKOFF.getService()));
            if(obj_s == null)
                return null;
            if(!ref.noReturnReflection(obj_s, "setBfChoice", Integer.class, 1)){
                log.error("{} API call failed for {}","setBfChoice", obj_s);
                return null;
            }
            if(!ref.noReturnReflection(obj_s , "setReason",String.class, reason)){
                log.error("{} API call failed for {}","setReason", obj_s);
                return null;
            }
        }
        Object obj_send = ((AssistantParent)obj_s).toFeature();
        //getCorrelator
        String correlator = entry.getCorrelator();
        if(correlator == null)
        {
            log.error("No correlator for device {}, something wrong!", entry.getDkey().toUdi());
            return null;
        }
        //set common info, pid, version , correlator
        setCommonInfo(obj_send, getRequestOrResponse(obj_send), entry, correlator,ServiceEnum.ODL_PNP_BACKOFF.getService());
        //send out xml string
        return xmlhandler.xmlGenerator(obj_send);

    }
    public String sendWorkInfo( IMasterDB entry) throws Exception{
        log.info("Sending WorkInfo...");
        String ser = ServiceEnum.ODL_PNP_WORKINFO.getService();
        Object obj_s = constructor.newInstance(services.getService(ser));
        if(obj_s == null)
            return null;
        Object info = ref.hasReturnReflection(obj_s, "getInfo");
        if(info == null)
        {
            log.error("No {} content in {}.", "info", obj_s);
            return null;
        }
        String correlator = entry.getCorrelator();
        if(correlator == null)
        {
            log.error("No correlator for device {}, something wrong!", entry.getDkey().toUdi());
            return null;
        }
        //set common info, pid, version , correlator
        setCommonInfo(obj_s, info, entry, correlator,ser);
        return xmlhandler.xmlGenerator(obj_s);
    }

    public Object getRequestOrResponse(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        Object req = null;
        if(obj == null){
            log.error("Object given is null.");
            return null;
        }

        if(obj.getClass().equals(services.getService(ServiceEnum.ODL_PNP_DEVICEID.getService()))){

            req =    ref.hasReturnReflection(obj,"getRequest");
            if(req == null){
                log.error("No {} content in {} ","request",obj);
                return null;
            }
        }else if(obj.getClass().equals(services.getService(ServiceEnum.ODL_PNP_FAULT.getService()))){
                 req = ref.hasReturnReflection(obj,"getResponse");
                 if(req == null){
                     log.error("No {} content in {} ","response",obj);
                     return null;
                 }
             }
        else{
            req = ref.hasReturnReflection(obj, "getRequestOrResponse");
            if(req == null){
                log.error("No {} content in {}", "request or response", obj);
                return null;
            }
        }
        return req;
    }

    public Boolean setCommonInfo(Object obj, Object req, IMasterDB entry, String correlator, String service) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        log.info("Correlator:{}",correlator);
        if(obj == null || req == null){
            log.error("Object to set common info is null");
            return false;
        }
        if(! ref.noReturnReflection(obj,"setVersion",String.class, entry.getVersion())){
            log.error("{} API call failed for {}","setVersion", obj);
            return false;
        }
        if(! ref.noReturnReflection(obj,"setUdi",String.class, entry.getDkey().toUdi())){
            log.error("{} API call failed for {}","setUdi", obj);
            return false;
        }
        if(entry.isAuthRequired()){
            if(entry.getUsr()!=null && entry.getPwd() != null){
                if(! ref.noReturnReflection(obj,"setUsr",String.class, entry.getUsr())){
                    log.error("{} API call failed for {}","setUsr", obj);
                    return false;
                }

                if(! ref.noReturnReflection(obj,"setPwd",String.class, entry.getPwd())){
                    log.error("{} API call failed for {}","setPwd", obj);
                    return false;
                }
            }else{
                if(service.equals(ServiceEnum.ODL_PNP_IMAGE_INSTALL.getService())
                        || service.equals(ServiceEnum.ODL_PNP_CONFIG_UPGRADE.getService())){
                    Integer noCheckTime = (Integer) ref.hasReturnReflection(obj, "getNoCheckTime");
                    String postReloadPriv = (String) ref.hasReturnReflection(obj, "getPostReloadPriv");
                    if(noCheckTime == null){
                        if(! ref.noReturnReflection(obj,"setNoCheckTime",Integer.class, MAXNOCHECKTIME)){
                            log.error("{} API call failed for {}","setNoCheckTime", obj);
                            return false;
                        }
                    }
                        if(! ref.noReturnReflection(obj,"setPostReloadPriv",String.class, POSTRELOADPREV)){
                            log.error("{} API call failed for {}","setPostReloadPrev", obj);
                            return false;
                        }
                  }
            }

        }
        log.info("Set correlator {} to req: {} ", correlator, req);
        if(!setCorrelator(req, correlator)){
            return false;
        }
        return true;
    }
    public Boolean updateErrorInfo(IMasterDB entry, Object res, String service) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException{
        if(res == null){
            log.error("Response object is null.");
            return false;
        }
        Object errorInfo = null;
        if(!service.equals(ServiceEnum.ODL_PNP_DEVICEINFO.getService()))
            errorInfo = ref.hasReturnReflection(res,"getErrorInfo");
        else{
            AssistantParent ap = (AssistantParent) services.getAssistant(ServiceEnum.ODL_PNP_DEVICEINFO.getService()).newInstance();
            errorInfo = ap.getErrorInfo(res);
        }

        if(errorInfo == null){
            log.error("No {} content in {}","errorInfo", res);
            return false;
        }
        Object tmpObj = ref.hasReturnReflection(errorInfo, "getErrorCode" );
        if(tmpObj != null){
            entry.setErrorCode((String)tmpObj);
        }
        tmpObj = ref.hasReturnReflection(errorInfo, "getErrorMessage" );
        if(tmpObj != null){
            entry.setErrorMessage((String)tmpObj);
        }
        tmpObj = ref.hasReturnReflection(errorInfo, "getErrorSeverity" );
        if(tmpObj != null){
            entry.setErrorSeverity((String)tmpObj);
        }
        if(!entry.getStatus().equals(DeviceStatus.ODL_PNP_DEPLOYED))
            entry.setStatus(DeviceStatus.ODL_PNP_ERROR);
        else{
            entry.setDynamicStatus(DeviceStatus.ODL_PNP_DAYTWO_JOB_ERROR);
        }
        return true;
    }
    public Boolean setCorrelator(Object req, String correlator) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        if(! ref.noReturnReflection(req,"setCorrelator",String.class, correlator)){
            log.error("{} API call failed for {}","setCorrelator", req);
            return false;
        }
        return true;
    }
    public String getCorrelator(Object req) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        Object corObj = ref.hasReturnReflection(req, "getCorrelator");
        if(corObj == null)
            return null;
        return (String)corObj;
    }
    public String getHeadUdi(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        Object udiObj = ref.hasReturnReflection(obj, "getUdi");
        if(udiObj == null)
            return null;
        return (String) udiObj;
    }
    public Boolean validateCorrelator(String correlator, IMasterDB entry){
        if(entry.getCorrelator() == null)
            return false;
        if(entry.getCorrelator().equals(correlator)){
            return true;
        }
        return false;
    }

}
