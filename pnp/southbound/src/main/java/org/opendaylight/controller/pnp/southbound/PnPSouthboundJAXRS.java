package org.opendaylight.controller.pnp.southbound;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.enunciate.jaxrs.ResponseCode;
import org.codehaus.enunciate.jaxrs.StatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/")
public class PnPSouthboundJAXRS {
    protected static final Logger logger = LoggerFactory
        .getLogger(PnPSouthboundJAXRS.class);
    protected PnPSouthboundImpl impl= new PnPSouthboundImpl();
    @Path("/WORK-REQUEST")
    @POST
    @Consumes({MediaType.TEXT_PLAIN, MediaType.TEXT_XML})
    @Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_XML})
    @StatusCodes({ @ResponseCode(code = 503, condition = "Destination unreachable") })
    public String parseWorkRequest(String inString) throws Exception {
        //    PnPSouthboundImpl impl = new PnPSouthboundImpl();
        return impl.RequestProcess(inString, true);
    }
    @Path("/WORK-RESPONSE")
    @POST
    @Consumes({MediaType.TEXT_PLAIN, MediaType.TEXT_XML})
    @Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_XML})
    @StatusCodes({ @ResponseCode(code = 404, condition =
                "The service was not found") })
    public String parseWorkResponse(String inString) throws Exception{
        //    PnPSouthboundImpl impl = new PnPSouthboundImpl();
        return impl.RequestProcess(inString, false);
    }
    @Path("/HELLO")
    @GET
    @StatusCodes({ @ResponseCode(code = 200, condition = "OK") })
    public Response parseHello() throws Exception{
        return  Response.ok().build();
    }
}
